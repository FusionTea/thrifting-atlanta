file_to_import = Rails.root.join('db/imports/store-import-2018-11-02.csv')
importer = StoreCsvImporter.import_file(file_to_import)
puts "############"
puts "IMPORTER STATS"
pp importer.stats
puts "############"
StoreAddressCoordinateBuilder.build_batch(
  importer.imported_stores
)

store_ids = importer.imported_stores.map(&:id)
Store.where(id: store_ids).update_all(active: true)
