# README

## Setting up Environment
* Create `.env` file
```
WEBPACKER_DEV_SERVER_HOST=webpacker
POSTGRES_USER=<db-user>
POSTGRES_PASSWORD=<db-password>
TA_DB_USER_NAME=<db-user>
TA_DB_PASSWORD=<db-password>
TA_DB_HOST=database
SECRET_KEY_BASE=<rails-secret-key>
```

* `docker-compose build` - Build docker images
* `docker-compose run web bin/rails db:create` - Create database

## Running Application
`docker-compose up`

## Running tests
`docker-compose run web bundle exec rspec`

## Entering Console
`docker-compose run web bin/rails c`

## Running Rails Commands
`docker-compose run web bin/rails <rails-command>`
