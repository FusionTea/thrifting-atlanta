Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  root to: 'home#index'

  devise_for :users,  class_name: 'User',
                      controllers: {
                        sessions: 'api/sessions',
                        registrations: 'api/registrations',
                        passwords: 'api/passwords'
                      },
                      modules: "api",
                      path_names: {
                                    sign_in: 'login',
                                    sign_out: 'logout'
                                  }

  namespace :api do

    devise_scope :user do

      # Session Management
      post 'session', to: 'sessions#create'
      delete 'session', to: 'sessions#destroy'

      post 'registrations', to: 'registrations#create'

      post 'passwords/reset', to: 'passwords#create_reset'

    end

    namespace :admin do

      resources :stores, only: [:index, :show, :update, :create]
      put 'stores/:id/cover_image', to: 'stores#update_cover_image', as: 'store_cover_image'

      resources :store_photos, only: [:create, :destroy]

    end

    resources :stores, only: [:index, :show, :create]
    get 'stores/codes/:code', to: 'stores#show_code'
    get 'stores/:id/cover_image', to: 'stores#cover_image'

    get 'stores/:store_id/reviews', to: 'store_reviews#index'
    post 'stores/:store_id/reviews', to: 'store_reviews#create'

    get 'stores/:store_id/store_photos', to: 'store_photos#index'

    get 'store_photos/:id', to: 'store_photos#show'
    get 'store_photos/:id/thumbnail', to: 'store_photos#thumbnail'

  end

  mount LetterOpenerWeb::Engine, at: "/letter_opener" if Rails.env.development?

  get '*path', to: 'home#index',
               constraints: lambda {|r|
                !r.original_fullpath
                  .include?('rails/active_storage') &&
                !r.original_fullpath
                  .include?('assets') &&
                !r.original_fullpath
                  .include?('packs')
                }

end
