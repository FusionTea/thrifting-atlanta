require 'csv'

class StoreCsvImporter
  ROW_IMPORTER = StoreCsvRowImporter

  def self.import_file(filepath)
    import_string(IO.read(filepath))
  end

  def self.import_string(csv_string)
    importer = self.new(csv_string)
    importer.import
    return importer
  end

  attr_reader :csv_string, :stats, :new_stores,
              :updated_stores, :imported_stores, :failed_rows

  def initialize(csv_string)
    @csv_string = csv_string
    @imported_stores = []
    @new_stores = []
    @updated_stores = []
    @failed_rows = []
    @stats = {}
  end

  def import
    import_rows
    generate_stats
  end

  private

  def import_rows
    @total_rows = 0
    ::CSV.parse(@csv_string, headers: true) do |row|
      @total_rows += 1
      import_row(row)
    end
  end

  def import_row(row)
    row_importer = ROW_IMPORTER.import(row)
    if row_importer.valid?
      @imported_stores.push(row_importer.store)
      @new_stores.push(row_importer.store) if row_importer.new?
      @updated_stores.push(row_importer.store) unless row_importer.new?
    else
      @failed_rows.push(row)
    end
  end

  def generate_stats
    @stats = {
      total_attempted: @total_rows,
      total_imported: @imported_stores.length,
      total_imported_new: @new_stores.length,
      total_imoprted_updated: @updated_stores.length,
      total_failed: @failed_rows.length,
    }
  end

end
