class StoreCsvRowImporter
  include ActiveModel::Validations

  def self.import(row)
    importer = self.new(row)
    importer.import
    return importer
  end

  attr_reader :store, :row

  validates_presence_of :row
  validate :validate_store, :validate_store_address

  def initialize(row)
    @row = row
    @status = 'in_progress'
    @built = false
  end

  def import
    @store = get_store
    @store.assign_attributes(store_attributes)
    if @store.save
      StoreTsvBuilder.build(store: @store)
    end
  end

  def new?
    @built
  end

  private

  def get_store
    find_store || build_store
  end

  def find_store
    Store.find_by(code: code)
  end

  def build_store
    @built = true
    Store.new(code: code)
  end

  def code
    @code ||= codify(@row['UNIQUE_CODE']) || generate_code
  end

  def generate_code
    codify(
      "#{@row['STORE_NAME']} #{@row['ADDR_LINE_1']} "\
      "#{@row['ADDR_CITY']} #{@row['ADDR_ZIP']}"
    )
  end

  def codify(string)
    return string if string.blank?
    string.downcase.strip.gsub(/[^0-9a-z]/i, '_')
  end

  def store_attributes
    @store_attributes ||= {
      name: @row['STORE_NAME'],
      description: @row['STORE_DESCRIPTION'],
      email: @row['EMAIL'],
      website_url: @row['WEBSITE_URL'],
      instagram_url: @row['INSTAGRAM_URL'],
      facebook_url: @row['FACEBOOK_URL'],
      twitter_url: @row['TWITTER_URL'],
      featured: @row['FEATURED'] || false,
      business_hour_info: @row['BUSINESS_HOURS'],
      address_attributes: address_attributes
    }
  end

  def address_attributes
    @address_attributes ||= existing_address_attributes.merge({
      'name' => "#{@row['STORE_NAME']}'s Address",
      'line_1' => @row['ADDR_LINE_1'],
      'line_2' => @row['ADDR_LINE_2'],
      'municipality' => @row['ADDR_CITY'],
      'region' => @row['ADDR_STATE'],
      'postal_code' => @row['ADDR_ZIP'],
      'country' => 'US'
    })
  end

  def existing_address_attributes
    (store.address && store.address.attributes) ||
      {}
  end

  def validate_store
    unless @store.valid?
      errors.add(:store, 'is invalid')
    end
  end

  def validate_store_address
    unless @store.address.valid?
      errors.add(:store_address, 'is invalid')
    end
  end

end
