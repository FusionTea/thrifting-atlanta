class UserMailer < ApplicationMailer
  layout 'mailer'

  def admin_pending_store
    subject = 'New Store'
    @store = Store.find_by_id(params[:store_id])
    url_options = ActionMailer::Base.default_url_options
    @url = UrlBuilder.build(path: "admin/stores/#{@store.id}")
    mail(to: params[:emails], subject: subject)
  end

end
