class ReviewSerializer < ActiveModel::Serializer
  attributes :id, :reviewer_name, :content, :status,
             :reviewer_email, :store_id, :date

  belongs_to :user

end
