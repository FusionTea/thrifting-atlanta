class StoreSerializer < ActiveModel::Serializer
  attributes  :id, :name, :code, :description, :status,
              :page_view_count, :phone, :email,
              :website_url, :facebook_url, :twitter_url,
              :instagram_url, :featured, :cover_image_filename,
              :longitude, :latitude, :business_hour_info, :reviews_count

  belongs_to :address
  has_many :tags

end
