class PhotoSerializer < ActiveModel::Serializer
  attributes :id, :description, :image_url,
             :thumbnail_url, :created_at

end
