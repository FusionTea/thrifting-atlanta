class ApiError < RuntimeError
  DEFAULT_STATUS = 400

  attr_reader :status, :data

  def initialize(options = {})
    @status = options[:status] || DEFAULT_STATUS
    @data = options[:data]
  end

  def json
    {
      message: message,
      status: @status,
      data: @data
    }
  end

end
