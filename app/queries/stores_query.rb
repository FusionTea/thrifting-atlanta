class StoresQuery
  DEFAULT_ORDER = 'ASC'
  DEFAULT_ORDER_BY = 'name'
  DEFAULT_PAGE = 1
  DEFAULT_PAGE_SIZE = 25
  DEFAULT_SERIALIZER = StoreSerializer
  COLLECTION_SERIALIZER = ActiveModel::Serializer::CollectionSerializer

  attr_reader :stores, :total_entries, :order,
              :order_by, :search_query, :code,
              :featured, :status, :region, :page,
              :page_size, :serializer

  def self.json(params = {})
    query = self.new(params)
    query.json
  end

  def self.execute(params = {})
    query = self.new(params)
    query.stores
  end

  def initialize(params = {})
    @stores = nil
    @total_entries = nil
    @page = params[:page] || DEFAULT_PAGE
    @page_size = params[:page_size] || DEFAULT_PAGE_SIZE
    @order_by = params[:order_by] || DEFAULT_ORDER_BY
    @order = params[:order] || DEFAULT_ORDER
    @featured = params[:featured]
    @status = params[:status]
    @region = params[:region]
    @search_query = params[:search_query]
    @code = params[:code]
    @serializer = params[:serializer] || DEFAULT_SERIALIZER
    execute
  end

  def json
    items = COLLECTION_SERIALIZER.new(stores, serializer: @serializer)
    {
      items: items,
      total_entries: @total_entries,
      page: @page,
      page_size: @page_size,
      order: @order,
      order_by: @order_by,
    }
  end

  private

  def execute
    @stores = Store.all.order(order_string)
    @stores = @stores.search(@search_query) if @search_query.present?
    @stores = @stores.featured if @featured.to_s == 'true'
    @stores = @stores.by_status(@status) if @status.present?
    @stores = @stores.by_region(@region) if @region.present?
    @stores = @stores.by_code(@code) if @code.present?
    @total_entries = stores.count
    @stores = @stores.paginate(page: page, page_size: page_size)
                    .includes(:address, :tags)
                    .with_attached_cover_image
  end

  def order_string
    if @order == 'RANDOM'
      'RANDOM()'
    else
      "stores.#{@order_by} #{@order}"
    end
  end

end
