class ReviewsQuery
  DEFAULT_ORDER = 'DESC'
  DEFAULT_ORDER_BY = 'date'
  DEFAULT_PAGE = 1
  DEFAULT_PAGE_SIZE = 25
  DEFAULT_SERIALIZER = ReviewSerializer
  COLLECTION_SERIALIZER = ActiveModel::Serializer::CollectionSerializer

  attr_reader :reviews, :total_entries, :order,
              :order_by, :store_id, :status

  def self.json(params = {})
    query = self.new(params)
    query.json
  end

  def self.execute(params = {})
    query = self.new(params)
    query.reviews
  end

  def initialize(params = {})
    @reviews = nil
    @total_entries = nil
    @page = params[:page] || DEFAULT_PAGE
    @page_size = params[:page_size] || DEFAULT_PAGE_SIZE
    @order_by = params[:order_by] || DEFAULT_ORDER_BY
    @order = params[:order] || DEFAULT_ORDER
    @status = params[:status]
    @store_id = params[:store_id]
    @serializer = params[:serializer] || DEFAULT_SERIALIZER
    execute
  end

  def json
    items = COLLECTION_SERIALIZER.new(@reviews, serializer: @serializer)
    {
      items: items,
      total_entries: @total_entries,
      page: @page,
      page_size: @page_size,
      order_by: @order_by
    }
  end

  private

  def execute
    @reviews = Review.all
    @reviews = @reviews.by_store(@store_id) if @store_id.present?
    @reviews = @reviews.by_status(@status) if @status.present?
    @total_entries = @reviews.count
    @reviews = @reviews.order("#{@order_by} #{@order}")
    @reviews = @reviews.paginate(
                page: @page, page_size: @page_size
              ).includes(:user)
  end

end
