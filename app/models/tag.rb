class Tag < ApplicationRecord
  DEFAULT_NAMES = ['Thrift', 'Consignment', 'Vintage', 'Plus Size',
                    'Kids', 'Womens', 'Mens', 'Furniture']

  has_many :store_tags, inverse_of: :tag

  validates_presence_of :name
end
