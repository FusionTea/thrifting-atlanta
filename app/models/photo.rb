class Photo < ApplicationRecord
  THUMBNAIL_SIZE = '200x200'

  has_one :store_photo
  has_one :store, through: :store_photo, inverse_of: :photos

  has_one_attached :image
  has_one_attached :thumbnail

  def image_url
    Rails.application.routes
          .url_helpers
          .rails_blob_path(
            image, only_path: true
          )
  end

  def thumbnail_url
    self.image
        .variant(
          resize: THUMBNAIL_SIZE
        )
        .processed
        .service_url
  end

end
