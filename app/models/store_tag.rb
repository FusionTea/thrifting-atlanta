class StoreTag < ApplicationRecord
  belongs_to :store, inverse_of: :store_tags
  belongs_to :tag, inverse_of: :store_tags

  validates_presence_of :store, :tag

end
