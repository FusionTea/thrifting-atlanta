class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true

  scope :paginate, -> (page_size: 25, page: 1) {
    page_size = page_size.to_i
    page = page.to_i
    offset = (page - 1)*page_size
    limit(page_size).offset(offset)
  }

end
