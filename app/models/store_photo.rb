class StorePhoto < ApplicationRecord
  belongs_to :store
  belongs_to :photo

  scope :by_store, -> (store_id) {where(store_id: store_id)}

  def image_url
    photo.image_url
  end

  def thumbnail_url
    photo.thumbnail_url
  end

  def description
    photo.description
  end

end
