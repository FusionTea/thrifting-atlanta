class User < ApplicationRecord
  ROLES = %w(member admin)
  ADMIN_ROLE = "admin"

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :reviews

  before_validation :set_defaults, on: :create

  validates_presence_of :first_name, :last_name, :email
                        :role
  validates_inclusion_of :role, in: ROLES

  def admin?
    role == ADMIN_ROLE
  end

  private

  def set_defaults
    self.role ||= :member
  end

end
