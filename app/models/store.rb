class Store < ApplicationRecord
  include PgSearch
  STATUSES = %w(active hidden archived pending)
  DEFAULT_BUSINESS_HOURS = "Monday - Friday: 8:00AM - 5:00PM\n"\
                           "Saturday: 10:00AM - 6:00PM\n"\
                           "Sunday: Closed"

  belongs_to :address, inverse_of: :store
  has_many :store_tags, inverse_of: :store
  has_many :tags, through: :store_tags
  has_many :reviews
  has_many :store_photos
  has_many :photos, through: :store_photos, inverse_of: :store
  has_one_attached :cover_image

  accepts_nested_attributes_for :address, :store_tags

  validates_presence_of :name, :code, :status
  validates_uniqueness_of :code
  validates :code, format: { with: /\A[a-z_0-9]+\z/, message: "contains illegal characters"}
  validates_inclusion_of :featured, in: [true, false]
  validates_inclusion_of :status, in: STATUSES

  before_validation :set_defaults, on: :create

  pg_search_scope :search, against: [
                                      :name,
                                      :description,
                                    ],
                            using: {
                              tsearch: {
                                tsvector_column: :tsv,
                                prefix: true,
                                dictionary: :english
                              }
                            }

  scope :active, -> {
    by_status(:active)
  }

  scope :hidden, -> {
    by_status(:hidden)
  }

  scope :pending, -> {
    by_status(:pending)
  }

  scope :by_status, -> (status) {
    where(status: status)
  }

  scope :featured, -> {
    where(featured: true)
  }

  scope :by_region, -> (region) {
    joins(:address)
    .merge(Address.by_region(region))
  }

  scope :by_tags, -> (tag_ids) {
    joins(:tags)
    .merge(Tag.where(id: tag_ids))
  }

  scope :by_code, -> (code) {
    where(code: code)
  }

  def cover_image_url
    return unless cover_image.attached?
    self.cover_image
        .variant(
          resize: '600x400'
        )
        .processed
        .service_url
  end

  def cover_image_filename
    return unless cover_image.attached?
    cover_image.filename
  end

  private

  def set_defaults
    self.page_view_count ||= 0
    self.status ||= :hidden
    self.business_hour_info ||= DEFAULT_BUSINESS_HOURS
    if self.address
      self.address.name ||= "#{self.name}'s Address"
      self.address.country ||= "US"
    end
    if self.code
      self.code = self.code.downcase.strip.gsub(/[^0-9a-z]/i, '_')
    end
  end

end
