class Review < ApplicationRecord
  STATUSES = %w(pending approved flagged)

  belongs_to :user, optional: true
  belongs_to :store, counter_cache: true

  before_validation :set_defaults

  validates_presence_of :status, :content, :date
  validates_inclusion_of :status, in: STATUSES
  validate :validate_reviewer

  scope :by_store, -> (store_id) {
    where(store_id: store_id)
  }

  scope :by_status, -> (status) {
    where(status: status)
  }

  private

  def set_defaults
    self.status ||= :approved
    self.date ||= Time.current
  end

  def validate_reviewer
    if self.user.blank? && self.reviewer_email.blank?
      self.errors.add(:reviewer_email, 'must be present when there is no associated user')
    end
    if self.user.blank? && self.reviewer_name.blank?
      self.errors.add(:reviewer_name, 'must be present when there is no associated user')
    end
  end

end
