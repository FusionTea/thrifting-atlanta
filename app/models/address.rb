class Address < ApplicationRecord
  validates_presence_of :name, :line_1, :country, :region, :municipality

  has_one :store, inverse_of: :address

  scope :by_region, -> (region) {
    where(region: region)
  }

end
