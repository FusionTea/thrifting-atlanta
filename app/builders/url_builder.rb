class UrlBuilder
  DEFAULT_PROTOCOL = 'http'

  def self.build(*args)
    builder = self.new(*args)
    builder.build
    return builder.url
  end

  attr_reader :url, :host, :port, :protocol

  def initialize(options = {})
    default_url_opts = ActionMailer::Base.default_url_options
    @protocol = options[:protocol] || default_url_opts[:protocol] ||
      DEFAULT_PROTOCOL
    @host = options[:host] || default_url_opts[:host]
    @port = options[:port] || default_url_opts[:port]
    @path = options[:path]
  end

  def build
    @url = "#{@protocol}://#{@host}#{port_str}/#{@path}"
  end

  private

  def port_str
    @port.blank? ? nil : ":#{@port}"
  end

end
