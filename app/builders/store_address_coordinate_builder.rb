class StoreAddressCoordinateBuilder
  BASE_URL = "https://maps.googleapis.com/maps/api/geocode/json"

  def self.build_batch(stores)
    stores.map{|s| self.build(s)}
  end

  def self.build(store)
    builder = self.new(store)
    builder.build
    return builder
  end

  attr_reader :store, :address, :latitude, :longitude, :response

  def initialize(store)
    @store = store
    @address = @store.address
  end

  def lat
    @latitude
  end

  def lng
    @longitude
  end

  def build
    get_coordinates
    assign_coordinates
  end

  private

  def get_coordinates
    @response = HTTParty.get(BASE_URL, {query: query})
    result = @response['results'].first
    geometry = result && result['geometry']

    return if geometry.blank?

    @latitude = geometry['location']['lat']
    @longitude = geometry['location']['lng']
  end

  def assign_coordinates
    if lat.present? && lng.present?
      store.update(
        latitude: latitude,
        longitude: longitude
      )
    end
  end

  def query
    {
      key: Rails.configuration.google_maps_api_key,
      address: address_string
    }
  end

  def address_string
    "#{@address.line_1} "\
    "#{@address.municipality} "\
    "#{@address.region} "\
    "#{@address.postal_code} "\
    "#{@address.country || 'US'}"
  end

end
