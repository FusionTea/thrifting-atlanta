class PhotoBuilder
  include ActiveModel::Validations

  attr_reader :image, :description, :photo

  validates_presence_of :image
  validate :validate_image_type

  def self.build(options = {})
    builder = self.new(options)
    builder.build
    return builder
  end

  def initialize(options = {})
    @image = options[:image]
    @description = options[:description]
  end

  def build
    return unless self.valid?

    @photo = Photo.create(description: @description)
    @photo.image.attach(@image)
  end

  private

  def validate_image_type
    return unless @image.present?

    unless @image.content_type.include?('image')
      errors.add(:image, 'has invalid type')
    end
  end

end
