class PendingStoreBuilder
  include ActiveModel::Validations

  attr_reader :store

  validate :validate_store, :validate_store_address

  def self.build(*args)
    builder = self.new(*args)
    builder.build
    return builder
  end

  def initialize(options = {})
    @store = Store.new(options)
    @store.status = 'pending'
    @store.featured = false
    @store.code = generate_code
  end

  def build
    return unless valid?
    if @store.save
      StoreTsvBuilder.build(store: @store)
      StoreAddressCoordinateBuilder.build(store)
      notify_admins
    end
  end

  private

  def notify_admins
    UserMailer.with(
      emails: admin_emails,
      store_id: @store.id
    ).admin_pending_store.deliver_now
  end

  def admin_emails
    User.where(role: :admin).pluck(:email)
  end

  def generate_code
    return if @store.address.blank?
    ("#{@store.name} #{@store.address.line_1} "\
    "#{@store.address.municipality} "\
    "#{@store.address.region} "\
    "#{@store.address.postal_code} "\
    "#{Time.current.strftime('%Y%m%d%H%M%S%L')}").downcase
      .strip.gsub(/[^0-9a-z]/i, '_')
  end

  def validate_store
    self.errors.add(:store, 'is invalid') unless store.valid?
  end

  def validate_store_address
    self.errors.add(:store_address, 'is missing') if store.address.blank?

    if store.address.present? && !store.address.valid?
      self.errors.add(:store_address, 'is invalid')
    end
  end

end
