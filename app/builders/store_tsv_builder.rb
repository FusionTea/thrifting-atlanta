class StoreTsvBuilder

  def self.build(*args)
    builder = self.new(*args)
    builder.build
    return builder
  end

  attr_reader :store, :tsv

  def initialize(options = {})
    @store = options[:store] || Store.find(options[:store_id])
    @tsv = nil
  end

  def build
    @tsv = generate_tsv
    @store.update_columns(tsv: @tsv)
  end

  private

  def generate_tsv
    ActiveRecord::Base.connection.execute(
      "SELECT to_tsvector('english', '#{tsv_string}') as tsv"
    ).first['tsv']
  end

  def tsv_string
    ActiveRecord::Base.connection.quote_string(
      "#{store_string} #{address_string} #{tag_string}"
    )
  end

  def store_string
    @store.attributes.slice('name', 'description').values.join(' ')
  end

  def address_string
    @store.address.attributes
      .slice('municipality', 'postal_code')
      .values.join(' ')
  end

  def tag_string
    @store.tags.pluck(:name).join(' ')
  end

end
