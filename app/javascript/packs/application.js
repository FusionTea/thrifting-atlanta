import "@babel/polyfill"

/* eslint no-console:0 */
// This file is automatically compiled by Webpack, along with any other files
// present in this directory. You're encouraged to place your actual application logic in
// a relevant structure within app/javascript and only use these pack files to reference
// that code so it'll be compiled.
//
// To reference this file, add <%= javascript_pack_tag 'application' %> to the appropriate
// layout file, like app/views/layouts/application.html.erb

import React from 'react'
import ReactDom from 'react-dom'
import axios from 'axios'
import {CsrfParam, CsrfToken} from 'csrf'
import {BrowserRouter as Router} from 'react-router-dom'
import Layout from 'components/layout'
import {createSession, destroySession} from 'api/session'
import OnDomContentLoad from 'util/on_dom_content_load'

const App = () =>(
  <Router>
    <Layout />
  </Router>
)

const bootApp = () => {
  axios.defaults.headers.common['x-csrf-token']=CsrfToken
  ReactDom.render(
    <App />,
    document.getElementById('main-body')
  )
}

OnDomContentLoad(bootApp)
