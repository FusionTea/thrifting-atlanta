import {postRequest, deleteRequest} from 'api/requests'

export const createSession = (options = {}) => {
  options.url = '/api/session'
  postRequest(options)
}

export const destroySession = (options = {}) => {
  options.url = '/api/session'
  deleteRequest(options)
}
