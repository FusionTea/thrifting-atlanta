import {getRequest, postRequest} from 'api/requests'

const STORES_ENDPOINT = '/api/stores'

export const getStores = (options={}) => {
  options.url = STORES_ENDPOINT
  getRequest(options)
}

export const getStore = (options={}) => {
  options.url = `${STORES_ENDPOINT}/${options.id}`
  getRequest(options)
}

export const getStoreByCode = (options={}) => {
  options.url = `${STORES_ENDPOINT}/codes/${options.code}`
  getRequest(options)
}

export const createStore = (options={}) => {
  options.url = STORES_ENDPOINT
  postRequest(options)
}
