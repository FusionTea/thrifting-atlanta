import {postRequest} from 'api/requests'

const PASSWD_RESET_URL = '/api/passwords/reset'

export const beginPasswordReset = (options = {}) => {
  options.url = PASSWD_RESET_URL
  postRequest(options)
}
