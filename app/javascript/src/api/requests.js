import axios from 'axios'

const tryFunc = (func, response) => {
  if(func != undefined){
    func(response)
  }
}

export const getRequest = (options={}) => {
  axios.get(options.url, {params: options.params})
        .then((response) => (tryFunc(options.success, response)))
        .catch((error) => (tryFunc(options.error, error)))

}

export const deleteRequest = (options={}) => {
  axios.delete(options.url)
        .then((response) => (tryFunc(options.success, response)))
        .catch((error) => (tryFunc(options.error, error)))
}

export const putRequest = (options={}) => {
  axios.put(options.url, options.params)
        .then((response) => (tryFunc(options.success, response)))
        .catch((error) => (tryFunc(options.error, error)))

}

export const postRequest = (options={}) => {
  axios.post(options.url, options.params)
        .then((response) => (tryFunc(options.success, response)))
        .catch((error) => (tryFunc(options.error, error)))
}
