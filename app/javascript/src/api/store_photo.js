import {getRequest, postRequest, deleteRequest} from 'api/requests'

export const getStorePhotos = (options = {}) => {
  options.url = `/api/stores/${options.storeId}/store_photos`
  getRequest(options)
}

export const createStorePhoto = (options = {}) => {
  options.url = `/api/admin/store_photos`
  postRequest(options)
}

export const deleteStorePhoto = (options = {}) => {
  options.url = `/api/admin/store_photos/${options.id}`
  deleteRequest(options)
}
