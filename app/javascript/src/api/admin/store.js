import {getRequest, putRequest, postRequest} from 'api/requests'

export const getAdminStores = (options={}) => {
  options.url = '/api/admin/stores'
  getRequest(options)
}

export const getAdminStore = (options={}) => {
  options.url = `/api/admin/stores/${options.id}`
  getRequest(options)
}

export const updateAdminStore = (options={}) => {
  options.url = `/api/admin/stores/${options.id}`
  putRequest(options)
}

export const updateAdminStoreCoverImage = (options={}) => {
  options.url = `/api/admin/stores/${options.id}/cover_image`
  putRequest(options)
}

export const createAdminStore = (options={}) => {
  options.url = `/api/admin/stores`
  postRequest(options)
}
