import {getRequest, postRequest} from 'api/requests'

export const getStoreReviews = (options = {}) => {
  options.url = `/api/stores/${options.storeId}/reviews`
  getRequest(options)
}

export const createStoreReview = (options = {}) => {
  options.url = `/api/stores/${options.storeId}/reviews`
  postRequest(options)
}
