import {postRequest} from 'api/requests'

const REGISTER_URL = '/api/registrations'
export const registerUser = (options = {}) => {
  options.url = REGISTER_URL
  postRequest(options)
}
