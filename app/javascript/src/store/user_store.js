import EventEmitter from 'wolfy87-eventemitter'
import {createSession, destroySession} from 'api/session'
import isBlank from 'util/is_blank'
import OnDomContentLoad from 'util/on_dom_content_load'

let emitter = new EventEmitter();

const getUserInfo = () => {
  let user = {loggedIn: false}
  let el = document.getElementById('user-info')
  if (el) {
    user.firstName = el.dataset.firstName
    user.lastName = el.dataset.lastName
    user.role = el.dataset.role
    user.loggedIn = true
  }
  return user
}

let user = {}

OnDomContentLoad(() => {
  user = getUserInfo()
})

class UserStoreClass {

  getUser() {
    return Object.assign({}, user)
  }

  loginUser(options = {}) {
    if(this.isLoggedIn()){return}

    let newOptions = {
      params: options.params,
      error: options.error,
      success: (response) => (this.successfulLogin(response, options.success))
    }
    createSession(newOptions)
  }

  isLoggedIn() {
    return (user.loggedIn == true)
  }

  isAdmin() {
    return (user.role === 'admin')
  }

  successfulLogin(response, callback) {
      let data = response.data
      data.loggedIn = true
      this.setUserData(data)
      callback(response)
  }

  setUserData(data) {
    user.firstName = data.first_name
    user.lastName = data.last_name
    user.role = data.role
    if (!isBlank(data.loggedIn)){
      user.loggedIn = data.loggedIn
    }
    emitter.emit('update')
  }

  logoutUser(options = {}) {
    let error = (err) => (console.log(err))
    destroySession({success: (response) => this.successfulLogout(response, options.success), error: options.error})
  }

  successfulLogout(response, callback) {
    user.loggedIn = false
    emitter.emit('update')
    if(callback){
      callback(response)
    }
  }

  subscribe(callback) {
    emitter.addListener('update', callback)
  }

  unsubscribe(callback) {
    emitter.removeListener('update', callback)
  }
}

const UserStore = new UserStoreClass()
export default UserStore
