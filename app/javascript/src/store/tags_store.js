import OnDomContentLoad from 'util/on_dom_content_load'
let tagsStore = {}

const readBootstrapTags = () => {
  let tagData = {}
  if (window.bootstrapData == null) {return tagData}
  let tagBootstrapData = window.bootstrapData.tags
  tagBootstrapData.forEach((tag) => {
    tagData[tag.id] = tag
  })
  return tagData
}

OnDomContentLoad(()=>{
  tagsStore = readBootstrapTags()
})


class TagsStoreClass {

  getTags() {
    return JSON.parse(JSON.stringify(tagsStore))
  }

  tagData(tagId) {
    return Object.assign({}, tagsStore[tagId])
  }
}


const TagsStore = new TagsStoreClass()
export default TagsStore
