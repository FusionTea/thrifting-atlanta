import EventEmitter from 'wolfy87-eventemitter'
import OnDomContentLoad from 'util/on_dom_content_load'

let emitter = new EventEmitter();
let notificationId = 0
let notifications = {}
const defaultDuration = 5 //seconds


class NotificationsStoreClass {

  getNotifications() {
    return Object.assign({}, notifications)
  }

  subscribe(callback) {
    emitter.addListener('update', callback)
  }

  unsubscribe(callback) {
    emitter.removeListener('update', callback)
  }

  add(options = {}) {
    const nextId = notificationId += 1
    const notification = {
      id: nextId,
      message: options.message,
      duration: options.duration,
      type: options.type
    }
    notifications[nextId] = notification
    emitter.emit('update')

    if (options.duration){
      setTimeout(() => {
        this.remove(nextId)
      }, options.duration * 1000)
    }
  }

  addSuccess(options = {}) {
    options.type = 'success'
    let duration = options.duration
    options.duration = Object.is(duration, undefined) ? defaultDuration : duration
    this.add(options)
  }

  addError(options = {}) {
    options.type = 'error'
    this.add(options)
  }

  addWarning(options = {}) {
    options.type = 'warning'
    let duration = options.duration
    options.duration = Object.is(duration, undefined) ? defaultDuration: duration
    this.add(options)
  }

  remove(notificationId) {
    delete notifications[notificationId]
    emitter.emit('update')
  }

}

const  NotificationsStore = new NotificationsStoreClass()
export default NotificationsStore

OnDomContentLoad(()=> {
  if (window.bootstrapData != null){
    let flashNotices = window.bootstrapData.flashNotices
    flashNotices.forEach((flashNotice) => {
      if (flashNotice[0] == 'danger'){
        NotificationsStore.addError({message: flashNotice[1]})
      }else if(flashNotice[0] == 'warning'){
        NotificationsStore.addWarning({message: flashNotice[1]})
      }else if(flashNotice[0] == 'success'){
        NotificationsStore.addSuccess({message: flashNotice[1]})
      }
    })
  }
})
