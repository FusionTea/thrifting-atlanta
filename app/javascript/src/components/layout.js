import React from 'react'
import {Route, Switch} from 'react-router-dom'
import ScrollToTopRoute from 'util/scroll_to_top_route.js'
import Navbar from 'components/common/navbar.js'
import Footer from 'components/common/footer.js'
import Login from 'components/login.js'
import ForgotPassword from 'components/forgot_password.js'
import SignUp from 'components/sign_up.js'
import Home from 'components/home/index.js'
import StoreIndex from 'components/store/index.js'
import StoreShow from 'components/store/show.js'
import StoreNew from 'components/store/new.js'
import AdminStoreIndex from 'components/admin/store/index.js'
import AdminStoreShow from 'components/admin/store/show.js'
import AdminStoreEdit from 'components/admin/store/edit.js'
import Notifications from 'components/common/notifications.js'

const Layout = (props)=> (
  <div id='main-js-body'>
    <Navbar />
    <Notifications />
    <div className='site-content'>
      <Switch>
        <ScrollToTopRoute exact path="/" component={Home} />
        <ScrollToTopRoute exact path="/stores" component={StoreIndex} />
        <ScrollToTopRoute exact path="/stores/new" component={StoreNew} />
        <ScrollToTopRoute exact path="/stores/:code" component={StoreShow} />
        <ScrollToTopRoute exact path="/login" component={Login} />
        <ScrollToTopRoute exact path="/forgot_password" component={ForgotPassword} />
        <ScrollToTopRoute exact path="/sign_up" component={SignUp} />
        <ScrollToTopRoute exact path="/admin/stores" component={AdminStoreIndex} />
        <ScrollToTopRoute exact path="/admin/stores/:id" component={AdminStoreShow} />
        <ScrollToTopRoute exact path="/admin/stores/:id/edit" component={AdminStoreEdit} />
        <ScrollToTopRoute component={MissingErrorPage} />
      </Switch>
    </div>
    <Footer />
  </div>
)

const MissingErrorPage = () => {
  return (
    <div>
      <div><em>{window.location.href}</em> does not exist.</div>
      <div>This isn't the store you're looking for</div>
    </div>
  )
}

export default Layout
