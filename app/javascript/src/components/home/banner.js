import React from 'react'
import {generateUrl} from 'util/history'
import {withRouter} from 'react-router-dom'
import {US} from 'store/countries_store'
import Button from 'components/common/button'
import StateDropdown from 'components/common/state_dropdown'

const backgroundUrl = '/three-women-shopping.jpg'
const backgroundStyle = {
  background: `url(${backgroundUrl}) no-repeat center center`,
  backgroundSize: 'cover',
  minHeight: '450px'
}
const overlayStyle = {
  backgroundColor: 'rgba(0,0,0, 0.40)',
  height: '100%',
  width: '100%',
  position: 'absolute',
  padding: '3rem 1.5rem'
}

class InitHomeBanner extends React.Component {

  constructor(props) {
    super()
    this.state = {
      query: '',
      region: ''
    }
    this.onInputChange = this.onInputChange.bind(this)
    this.onKeyPress = this.onKeyPress.bind(this)
    this.search = this.search.bind(this)
  }

  onInputChange(event) {
    let value = event.target.value
    this.setState({
      [event.target.name]: value,
    })
  }

  onKeyPress(event) {
    if(event.key === 'Enter'){
      this.search()
    }
  }

  search() {
    let params = this.params()
    let url = generateUrl('/stores', params)
    this.props.history.push(url)
  }

  params() {
    let params = {}
    if (this.state.region != ''){
      params.region = this.state.region
    }
    if (this.state.query != ''){
      params.search_query = this.state.query.replace(/^\s+|\s+$/g, '')
    }
    return params
  }

  render(){
    return(
      <section id='home-banner-section'
               className='section'
               style={backgroundStyle}>
        <div style={overlayStyle}>
          <div className='full-size dead-center-wrapper'>
            <div>
              <div className='column full-width has-text-centered'>
                <div className='banner-title'>
                  Find The Resale Store For You
                </div>
              </div>
              <div className='columns is-multiline is-mobile'>

                <div className='column'>
                  <div className='field is-inline-block base-side-margin'>
                    <p className='control has-icons-right'>
                      <input className='input is-rounded'
                              type='text'
                              placeholder='Search Stores'
                              style={{minWidth: '250px'}}
                              value={this.state.query}
                              onChange={this.onInputChange}
                              onKeyPress={this.onKeyPress}
                              name='query'
                      />
                      <span className='icon is-small is-right'>
                        <i className='fas fa-search'></i>
                      </span>
                    </p>
                  </div>
                </div>

                <div className='column'>
                  <div className='select'>
                    <StateDropdown value={this.state.region}
                                   name='region'
                                   onChange={this.onInputChange} />
                  </div>
                </div>

                <div className='column'>
                  <Button className='button pink-button'
                          onClick={this.search}>
                    Search
                  </Button>
                </div>

              </div>
            </div>
          </div>
        </div>
      </section>
    )
  }
}

const HomeBanner = withRouter(InitHomeBanner)

export default HomeBanner
