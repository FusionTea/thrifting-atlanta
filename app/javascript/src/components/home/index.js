import React from 'react'
import Head from 'components/common/head'
import HomeBanner from 'components/home/banner'
import FeaturedStores from 'components/home/featured_stores'
import About from 'components/home/about'
import {withRouter} from 'react-router-dom'

const Home = () => (
  <div id='home-page'>
    <Head title='Home'/>
    <HomeBanner />
    <FeaturedStores />
    <About />
  </div>
)

export default Home
