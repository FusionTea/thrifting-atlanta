import React from 'react'
import {Link} from 'react-router-dom'
import Loader from 'components/common/loader'
import StoreCard from 'components/store/card'
import {getStores} from 'api/store'

const PARAMS = {
  featured: true,
  page_size: 3,
  page: 1,
  order: 'RANDOM'
}

export default class FeaturedStores extends React.Component {

  constructor(props) {
    super()
    this.state = {
      loading: true
    }
  }

  componentWillMount() {
    getStores({
      params: PARAMS,
      success: (response) => {
        this.setState({
          stores: response.data.items,
          loading: false
        })
      }
    })
  }

  render(){
    return(
      <section id='featured-stores-section'
               className='section'>
        <div className='container'>
          <h2 className='title'>Featured Stores</h2>
            {this.state.loading ?
              <Loader text='Loading Featured Stores'
                      textStyle={{color: 'white'}}/>
              :
              <div>
                <div className='columns is-centered'>
                  {this.state.stores.map((store) => (
                    <StoreCard key={store.id}
                               store={store}
                               className='column'/>
                  ))}
                </div>
                <div className='full-width dead-center-wrapper'>
                  <Link to={'/stores?featured=true'}
                        className='button white-button has-text-center'
                        id='view-all-featured-stores'>
                    View All
                  </Link>
                </div>
              </div>
            }
        </div>
      </section>
    )
  }

}
