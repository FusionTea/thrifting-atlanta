import React from 'react'

const aboutImageUrl = '/keren-charles.jpg'
const aboutText = `
  Welcome to The Resale District, your one-stop guide to
  discover must-visit thrift, consignment and vintage stores
  in your neighborhood or nationwide! Our publisher Keren
  Charles Duclosel, is a nationally recognized influencer in
  the resale industry. She has been featured on CNN, FOX and
  listed by Essence Magazine as one of the Bloggers to Follow.
  She created this guide to share her love of thrift shopping
  and inspire you to experience the resale industry.
`

const About = () => (
  <section id='about-section'
           className='section'>
    <div className='container'>
      <h2 className='title'>
        About The Resale District
      </h2>
      <div className='columns'>
        <div className='column is-narrow'>
          <img src={aboutImageUrl}
               className='about-ta-image roundify'/>
        </div>
        <div className='column'>
          <div className='dead-center-wrapper'
               style={{height: '100%'}}>
            <div>{aboutText}</div>
          </div>
        </div>
      </div>
    </div>
  </section>
)

export default About
