import React from 'react'
import StorePhotoCollection from 'components/store/photo_collection'

const AdminStorePhotoCollection = ({store}) => (
  <StorePhotoCollection storeId={store.id}
                        addPhoto={true}
                        deletePhoto={true}/>
)

export default AdminStorePhotoCollection
