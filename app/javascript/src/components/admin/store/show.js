import React from 'react'
import {Link} from 'react-router-dom'
import {getAdminStore} from 'api/admin/store'
import {AuthorizeAdmin} from 'components/common/authorize_user'
import Button from 'components/common/button'
import Loader from 'components/common/loader'
import AdminStoreBusinessInfo from 'components/admin/store/business_info'
import AdminStoreLocation from 'components/admin/store/location'
import NotificationsStore from 'store/notifications_store'
import AdminStoreEditLocation from 'components/admin/store/edit_location'
import AdminStoreCoverImageInput from 'components/admin/store/cover_image_input'
import AdminStorePhotoCollection from 'components/admin/store/photo_collection'

const LOAD_STORE_ERROR = 'Uh-oh, could not load store. Try refreshing page.'

export default class AdminStoreShow extends React.Component {

  constructor(props) {
    super()
    this.state = {
      loading: true,
      store: null,
      showEditLocationModal: false
    }

    this.setStore = this.setStore.bind(this)
    this.onError = this.onError.bind(this)
    this.toggleEditLocationModal = this.toggleEditLocationModal.bind(this)
    this.updateStore = this.updateStore.bind(this)
  }

  componentWillMount() {
    let id = this.props.match.params.id
    getAdminStore({
      id: id,
      success: this.setStore,
      error: this.onError
    })
  }

  setStore(response) {
    this.setState({
      store: response.data,
      loading: false
    })
  }

  onError(repsonse) {
    NotificationsStore.addError({
      message: LOAD_STORE_ERROR
    })
  }

  toggleEditLocationModal() {
    this.setState((prevState) => (
      {
        showEditLocationModal: !prevState.showEditLocationModal
      }
    ))
  }

  updateStore(attributes) {
    this.setState({
      store: attributes
    })
  }

  render() {
    if (this.state.loading){
      return(
        <div className='full-width'>
          <AuthorizeAdmin/>
          <Loader text='Loading store info'/>
        </div>
      )
    }else{
      let store = this.state.store
      return(
        <div className='container'>
          <section className='hero'>
            <div className='hero-body'>
              <h1 className='title'>{store.name}</h1>
              <h2 className='subtitle'>
                {store.page_view_count} Page Views
              </h2>
            </div>
          </section>
          <div className='admin-section'>
            <div className='admin-section-header columns'>
              <h3 className='column is-narrow'>Business Information</h3>
              <div className='column is-narrow'>
                <Link to={`/admin/stores/${store.id}/edit`}>
                  <Button className='button pink-button'>Edit</Button>
                </Link>
              </div>
            </div>
            <AdminStoreBusinessInfo store={store}/>
          </div>

          <div className='admin-section'>
            <div className='admin-section-header columns'>
              <h3 className='column is-narrow'>
                Store Location
              </h3>
              <div className='column is-narrow'>
                <Button className='button pink-button'
                        onClick={this.toggleEditLocationModal}>
                  Edit
                </Button>
              </div>
            </div>
            <AdminStoreLocation store={store}/>
            {this.state.showEditLocationModal &&
              <AdminStoreEditLocation
                store={store}
                updateStore={this.updateStore}
                cancelLocationEdit={this.toggleEditLocationModal}/>
            }
          </div>
          <div className='admin-section'>
            <div className='admin-section-header'>
              <h3>
                Store Photos
              </h3>
            </div>
            <AdminStoreCoverImageInput  cover_image_filename={store.cover_image_filename}
                                        storeId={store.id}
                                        />
            <AdminStorePhotoCollection store={store}/>
          </div> </div>
      )
    }
  }

}
