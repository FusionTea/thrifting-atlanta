import React from 'react'

import {createAdminStore} from 'api/admin/store'
import codify from 'util/codify'
import NotificationsStore from 'store/notifications_store'

import Button from 'components/common/button'
import Notification from 'components/common/notification'
import {Input} from 'components/common/input'
import StoreAddrressInputs from 'components/store/address_inputs.js'
import StoreMapInputs from 'components/store/map_inputs.js'

export default class AdminStoreCreateModal extends React.Component {

  constructor(props) {
    super()
    this.state = this.newStoreData()
    this.state.active = false
    this.openModal=this.openModal.bind(this)
    this.nameChange=this.nameChange.bind(this)
    this.codeChange=this.codeChange.bind(this)
    this.closeModal=this.closeModal.bind(this)
    this.createStore=this.createStore.bind(this)
    this.updateAddress=this.updateAddress.bind(this)
    this.successCreate=this.successCreate.bind(this)
    this.errorCreate=this.errorCreate.bind(this)
    this.clearErrorMessage=this.clearErrorMessage.bind(this)
    this.updateCordinates = this.updateCordinates.bind(this)
  }

  newStoreData() {
    return {
      error: '',
      name: '',
      code: '',
      address: {
        region: 'AL'
      },
      longitude: null,
      latitude: null,
    }
  }

  newStore() {
    this.setState(this.newStoreData())
  }

  openModal() {
    this.setState({active: true})
  }

  closeModal() {
    this.setState({active: false})
  }

  nameChange(event) {
    let name = event.target.value
    let code = codify(name)

    this.setState({
      name: name,
      code: code
    })
  }

  codeChange(event) {
    let unparsed = event.target.value
    let code = codify(unparsed)
    this.setState({code: code})
  }

  updateAddress(address) {
    this.setState({address: address})
  }

  updateCordinates(event) {
    this.setState({
      latitude: event.latLng.lat(),
      longitude: event.latLng.lng()
    })
  }

  markerPosition() {
    return {
      lat: Number(this.state.latitude),
      lng: Number(this.state.longitude)
    }
  }

  params() {
    return {
      name: this.state.name,
      code: this.state.code,
      longitude: this.state.longitude,
      latitude: this.state.latitude,
      address_attributes: this.state.address
    }
  }

  createStore(){
    createAdminStore({
      params: this.params(),
      success: this.successCreate,
      error: this.errorCreate
    })
  }

  successCreate(response) {
    this.closeModal()
    this.newStore()
    this.props.onSuccessCreate(response.data)
  }

  errorCreate(error) {
    let errorMessage = error.response.data.message
    this.setState({error: errorMessage})
  }

  clearErrorMessage(error) {
    this.setState({error: ''})
  }

  validCreate() {
    return (
      this.state.name &&
      this.state.code &&
      this.state.address.line_1 &&
      this.state.address.municipality &&
      this.state.address.region &&
      this.state.address.postal_code
    )
  }

  render() {
    let modalClass = 'modal'
    if (this.state.active) {modalClass += ' is-active'}
    let codeHelper = `*Must only contain alphanumeric characters
                      and underscores`
    return (
      <div>
        <Button className='button yellow-button'
                onClick={this.openModal}>
          <span className='icon'>
            <i className='fas fa-plus'></i>
          </span>
          <span>
            Add Store
          </span>
        </Button>

        <div className={modalClass}>
          <div className='modal-background'></div>
          <div className='modal-card'>
            <header className='modal-card-head'>
              <p className='modal-card-title'>Create Store</p>
              <Button className='delete' aria-label='close' onClick={this.closeModal}></Button>
            </header>
            <section className='modal-card-body'>
                {this.state.error &&
                  <Notification notification={{
                                  type: 'error',
                                  message: this.state.error
                                }}
                                removeNotification={this.clearErrorMessage} />

                }
                <Input  label='Name'
                        inputProps={{
                          name: 'name',
                          onChange: this.nameChange,
                          value: this.state.name,
                          placeholder: 'Store Name'
                      }}/>
                <Input  label='Unique Code'
                        helper={codeHelper}
                        inputProps={{
                          name: 'code',
                          onChange: this.codeChange,
                          value: this.state.code,
                          placeholder: 'unique_store_code'
                      }}/>
                <StoreAddrressInputs address={this.state.address}
                                  updateAddress={this.updateAddress}/>
                <strong>Select Location On Map</strong>:<br/>
                <strong>Lat:</strong><span className='color-black'>{this.state.latitude}</span>
                <br/>
                <strong>Lng:</strong><span className='color-black'>{this.state.longitude}</span>
                <StoreMapInputs markerPosition={this.markerPosition()}
                                     onMapClick={this.updateCordinates}/>
            </section>
            <footer className='modal-card-foot'>
              <div className='field is-grouped'>
                <Button disabled={!this.validCreate()}
                        className='button pink-button is-link'
                        onClick={this.createStore} >Create Store</Button>
                <Button className='button is-text' onClick={this.closeModal}>Cancel</Button>
              </div>
            </footer>
          </div>
        </div>
      </div>
    )
  }
}
