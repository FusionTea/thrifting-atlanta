import React from 'react'

//Util
import {updateAdminStoreCoverImage} from 'api/admin/store'

//Store
import NotificationsStore from 'store/notifications_store'

//Components
import Loader from 'components/common/loader'
import {File} from 'components/common/input'

const imagePreviewStyle = {
  maxHeight: 250,
  margin: '0.5rem auto',
  width: 'auto'
}

export default class AdminStoreCoverImageInput extends React.Component {

  constructor(props) {
    super()

    this.state = {
      cover_image: '',
      cover_image_filename: props.cover_image_filename,
      cover_image_url: `/api/stores/${props.storeId}/cover_image`,
      uploading: false
    }

    this.handleChange=this.handleChange.bind(this)
    this.updateSuccess=this.updateSuccess.bind(this)
  }

  updateCoverImage() {
    let formData = new FormData()
    formData.append('cover_image', this.state.cover_image)
    updateAdminStoreCoverImage({
      id: this.props.storeId,
      params: formData,
      success: this.updateSuccess
    })
  }

  updateSuccess(response) {
    NotificationsStore.addSuccess({message: 'Updated Cover Image'})
    this.setState({uploading: false})
  }

  updateError(error) {
    NotificationsStore.addError({message: "Error Updating Cover Image"})
  }

  handleChange(event) {
    event.preventDefault()
    let reader = new FileReader()
    let file = event.target.files[0]
    this.setState({cover_image_filename: file.name})
    reader.onloadend = () => {
      this.setState({
        cover_image: file,
        cover_image_url: reader.result,
        uploading: true
      }, this.updateCoverImage)
    }

    reader.readAsDataURL(file)
  }

  render() {
    return(
      <div className='input-group'>
        <h2 className='subtitle'>Cover Image</h2>
        {this.state.uploading ?
        <Loader text={`Uploading ${this.state.cover_image_filename}`}/>
        :
        <div>
          <File label='Upload Cover Image'
                fileName={this.state.cover_image_filename}
                inputProps={{
                  onChange: this.handleChange
                }}/>
          {this.state.cover_image_url &&
            <img src={this.state.cover_image_url} style={imagePreviewStyle} alt="Cover Image Preview.."/>}
        </div>
        }
      </div>
    )
  }


}
