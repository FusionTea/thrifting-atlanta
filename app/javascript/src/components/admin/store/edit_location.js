import React from 'react'

import {updateAdminStore} from 'api/admin/store'

import StoreAddrressInputs from 'components/store/address_inputs'
import StoreMapInputs from 'components/store/map_inputs'
import Button from 'components/common/button'

import NotificationsStore from 'store/notifications_store'

export default class AdminStoreEditLocation extends React.Component {

  constructor(props){
    super()
    this.state = {
      longitude: props.store.longitude,
      latitude: props.store.latitude,
      address: props.store.address,
      mode: 'input'
    }
    this.submitUpdate = this.submitUpdate.bind(this)
    this.updateAddress = this.updateAddress.bind(this)
    this.onMapClick = this.onMapClick.bind(this)
    this.updateCoordinates = this.updateCoordinates.bind(this)
    this.errorSaveStore = this.errorSaveStore.bind(this)
    this.successSaveStore = this.successSaveStore.bind(this)
  }

  submitUpdate(event) {
    this.setState({
      mode: 'submitting'
    })
    updateAdminStore({
      id: this.props.store.id,
      params: this.params(),
      success: this.successSaveStore,
      error: this.errorSaveStore
    })
  }

  params(){
    return {
      latitude: this.state.latitude,
      longitude: this.state.longitude,
      address_attributes: this.state.address
    }
  }

  successSaveStore(response) {
    NotificationsStore.addSuccess({message: 'Successfully upated location'})
    this.props.updateStore(response.data)
    this.props.cancelLocationEdit()
  }

  errorSaveStore(error) {
    NotificationsStore.addError({message: 'Unsucessful updating location'})
  }

  updateAddress(address) {
    this.setState({address: address})
  }

  onMapClick(event) {
    const coords = event.latLng
    this.updateCoordinates(coords)
  }

  updateCoordinates(coords) {
    this.setState({
      latitude: coords.lat(),
      longitude: coords.lng()
    })
  }

  submitDisabled() {
    return this.state.mode == 'submitting'
  }

  markerPosition() {
    return {
      lat: Number(this.state.latitude),
      lng: Number(this.state.longitude)
    }
  }

  render() {
    return(
      <div className='modal is-active'>
        <div className='modal-background'
             onClick={this.props.cancelLocationEdit} />
        <div className='modal-card'>
          <header className='modal-card-head'>
            <p className='modal-card-title'>
              Edit Store Location
            </p>
            <Button className='delete'
                    aria-label='close'
                    onClick={this.props.cancelLocationEdit}/>
          </header>
          <section className='modal-card-body'>
            <StoreAddrressInputs
                address={this.state.address}
                updateAddress={this.updateAddress}/>
            <strong>Select Location On Map</strong>:<br/>
            <strong>Lat:</strong> {this.state.latitude}<br/>
            <strong>Lng:</strong> {this.state.longitude}
            <StoreMapInputs
                markerPosition={this.markerPosition()}
                updateCoordinates={this.updateCoordinates}
                onMapClick={this.onMapClick}/>
          </section>
          <footer className='modal-card-foot'>
            <div className='field is-grouped'>
              <Button className='button is-text'
                      onClick={this.props.cancelLocationEdit}>
                  Cancel
              </Button>
              <Button className='button pink-button is-link'
                      onClick={this.submitUpdate}
                      disabled={this.submitDisabled()}>
                Submit
              </Button>
            </div>
          </footer>
        </div>
      </div>
    )
  }
}

