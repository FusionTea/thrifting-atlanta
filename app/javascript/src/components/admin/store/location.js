import React from 'react'
import StoreMap from 'components/store/map'

const AdminStoreLocation = ({store}) => (
  <div className='full-width'>
    <div className='columns'>
      <div className='column is-narrow'>
        <span className='icon color-pink'>
          <i className='fas fa-map-marker'/>
        </span>
      </div>
      <div className='column'>
        <div>
          {store.address.line_1}
        </div>
        {store.address.line_2 &&
          <div>
            {store.address.line_2}
          </div>
        }
        <div>
          {store.address.municipality}, {' '}
          {store.address.region}, {' '}
          {store.address.postal_code}
        </div>

      </div>
    </div>
    <StoreMap store={store}/>
  </div>
)

export default AdminStoreLocation
