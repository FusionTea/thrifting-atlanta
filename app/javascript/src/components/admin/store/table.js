import React from 'react'
import {Link} from 'react-router-dom'
import NoResults from 'components/common/no_results'
import Pagination from 'components/common/pagination'

const tableRow = (store) => (
  <tr key={store.id}>
    <td><StoreLink store={store}/></td>
    <td>{store.address.region}, {store.address.postal_code}</td>
    <td>{store.tags.map((tag)=>(tag.name)).join(', ')}</td>
    <td>{store.page_view_count}</td>
  </tr>
)

const storeColumn = (store) => (
  <div key={store.id} className='column store-card'>
    <div className='box'>
      <div className='store-name'>
       <StoreLink store={store}/>
      </div>
      <div className='store-address'>
        {store.address.region}, {store.address.postal_code}
      </div>
      <div className='store-tags'>
        {store.tags.map((tag)=>(tag.name)).join(', ')}
      </div>
    </div>
  </div>
)

const StoreLink = ({store}) => (
  <Link to={`/admin/stores/${store.id}`}>
    {store.name}
  </Link>
)

const Table = ({stores}) => (
  <div className='full-width'>
    <table className='table full-width is-hidden-mobile'>
      <thead>
        <tr>
          <th>Name</th>
          <th>Address</th>
          <th>Tags</th>
          <th>Views</th>
        </tr>
      </thead>
      <tbody>
        {stores.map(tableRow)}
      </tbody>
    </table>
    <div className='columns is-hidden-tablet'>
      {stores.map(storeColumn)}
    </div>
  </div>
)
const AdminStoreTable = (props) => (
  <div className='full-width'>
    {(props.stores.length > 0) ?
      <Table stores={props.stores}/>
      :
      <NoResults />
    }
    <Pagination {...props}/>
  </div>
)

export default AdminStoreTable
