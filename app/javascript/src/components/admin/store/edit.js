import React from 'react'

//Util
import {goBack} from 'util/history'
import {getAdminStore, updateAdminStore} from 'api/admin/store'

//Stores
import NotificationsStore from 'store/notifications_store'
import TagsStore from 'store/tags_store'

//Components
import {AuthorizeAdmin} from 'components/common/authorize_user'
import Loader from 'components/common/loader'
import {Input, Textarea, Checkbox, Select} from 'components/common/input'
import Button from 'components/common/button'
import StoreTagInputs from 'components/store/tag_inputs.js'

export default class AdminStoreEdit extends React.Component{

  constructor(props) {
    super()

    this.state = {
      name: '',
      status: '',
      description: '',
      business_hour_info: '',
      email: '',
      phone: '',
      website_url: '',
      twitter_url: '',
      featured: '',
      address: '',
      latitude: null,
      longitude: null,
      tags: {},
      loading: true
    }

    this.setStore = this.setStore.bind(this)
    this.updateAddress = this.updateAddress.bind(this)
    this.updateTags = this.updateTags.bind(this)
    this.updateFeatured = this.updateFeatured.bind(this)
    this.textChange = this.textChange.bind(this)
    this.submit = this.submit.bind(this)
    this.navigateBack = this.navigateBack.bind(this)
    this.successSaveStore = this.successSaveStore.bind(this)
    this.updateCordinates = this.updateCordinates.bind(this)
  }

  componentWillMount() {
    let id = this.props.match.params.id
    getAdminStore({
      id: id,
      success: this.setStore,
      error: (response) => {
        NotificationsStore.addError({message: 'Something Happened'})
      }
    })
  }

  setStore(response) {
    let data = response.data
    let tags = TagsStore.getTags()
    Object.entries(tags).forEach(([id, tagData]) => {
      tags[id] = false
    })
    data.tags.forEach((tag)=>(tags[tag.id] = true))
    this.setState({
      name: data.name,
      status: data.status,
      description: data.description,
      business_hour_info: data.business_hour_info,
      email: data.email,
      phone: data.phone,
      facebook_url: data.facebook_url,
      twitter_url: data.twitter_url,
      instagram_url: data.instagram_url,
      website_url: data.website_url,
      featured: data.featured,
      address: data.address,
      longitude: data.longitude,
      latitude: data.latitude,
      cover_image_filename: data.cover_image_filename,
      tags: tags,
      loading: false
    })
  }

  params() {
    let formData = new FormData()
    let params = {
      name:  this.state.name,
      status:  this.state.status,
      description:  this.state.description,
      business_hour_info: this.state.business_hour_info,
      email: this.state.email,
      phone: this.state.phone,
      facebook_url:  this.state.facebook_url,
      twitter_url:  this.state.twitter_url,
      instagram_url:  this.state.instagram_url,
      website_url:  this.state.website_url,
      address_attributes:  this.state.address,
      longitude: this.state.longitude,
      latitude: this.state.latitude,
      tag_ids: this.selectedTagIds(),
      featured:  this.state.featured
    }
    return params
  }

  selectedTagIds() {
    return Object.entries(this.state.tags)
                  .filter(([tagId, checked]) => checked)
                  .map(([tagId, checked]) => tagId)

  }

  storeId() {
    return this.props.match.params.id
  }

  saveStore() {
    updateAdminStore({
      id: this.storeId(),
      params: this.params(),
      success: this.successSaveStore,
      error: this.errorSaveStore,
    })
  }

  successSaveStore(response) {
    NotificationsStore.addSuccess({
      message: `Successfuly Updated ${this.state.name}!`
    })
    goBack()
  }

  errorSaveStore(error) {
    NotificationsStore.addError({message: 'Unsuccessful Updating Store'})
  }

  updateAddress(address) {
    this.setState({address: address})
  }

  updateTags(tags) {
    this.setState({tags: tags})
  }

  updateCordinates(event) {
    this.setState({
      latitude: event.latLng.lat(),
      longitude: event.latLng.lng()
    })
  }

  updateFeatured(event) {
    let value = event.currentTarget.checked
    this.setState({featured: value})
  }

  textChange(event) {
    let value = event.target.value
    let name = event.target.name
    this.setState({[name]: value})
  }

  submit() {
    this.saveStore()
  }

  navigateBack() {
    goBack()
  }

  markerPosition() {
    return {
      lat: Number(this.state.latitude),
      lng: Number(this.state.longitude)
    }
  }

  render() {
    return(
      <div className='container'>
        <AuthorizeAdmin/>
        <section className='hero'>
          <div className='hero-body'>
            <h1 className='title'>Edit {this.state.name || 'Store'}</h1>
          </div>
        </section>
        {this.state.loading ?
          <Loader text='Loading Store Info'/>
          :
          <div className='store'>
            <div className='columns'>
              <div className='column'>
                <Input  label='Name'
                        required={true}
                        inputProps={{
                          name: 'name',
                          onChange: this.textChange,
                          value: this.state.name,
                          placeholder: 'Store Name'
                      }}/>
              </div>
              <div className='column is-narrow'>
                <Select label='Status'
                  inputProps={{
                    name: 'status',
                    onChange: this.textChange,
                    value: this.state.status || '',
                    options: {Active: 'active', Hidden: 'hidden', Pending: 'pending'}
                  }}/>
              </div>
              <div className='column'>
                <Input  label='Email'
                        leftIcon='fas fa-envelope'
                        inputProps={{
                          name: 'email',
                          onChange: this.textChange,
                          value: this.state.email || '',
                          placeholder: 'store@email.com'
                      }}/>
              </div>
            </div>
            <div className='columns'>
              <div className='column is-two-thirds'>
                <Textarea label='Description'
                          inputProps={{
                            name: 'description',
                            onChange: this.textChange,
                            value: this.state.description || '',
                            placeholder: 'Description Of Store'
                          }}/>
              </div>
              <div className='column'>
                <Textarea label='Business Hours Info'
                          inputProps={{
                            name: 'business_hour_info',
                            onChange: this.textChange,
                            value: this.state.business_hour_info || '',
                            placeholder: 'Monday-Friday: 8:00 AM - 5:00 PM\nSaturday-Sunday: Closed'
                          }}/>
                </div>
            </div>
            <Checkbox label={<strong>Featured</strong>}
                      inputProps={{
                        onChange: this.updateFeatured,
                        checked: this.state.featured,
                      }}/>
            <StoreTagInputs tags={this.state.tags}
                updateTags={this.updateTags}
            />

            <div className='columns'>
              <div className='column'>
                <div className='input-group'>
                  <h2 className='subtitle'>URLS</h2>
                  <Input  label='Phone'
                          leftIcon='fas fa-phone'
                          inputProps={{
                            name: 'phone',
                            onChange: this.textChange,
                            value: this.state.phone || '',
                            placeholder: '312-123-1234'
                        }}/>
                  <Input  label='Website Url'
                          leftIcon='far fa-globe'
                          inputProps={{
                            name: 'website_url',
                            onChange: this.textChange,
                            value: this.state.website_url || '',
                            placeholder: 'www.yourstore.com'
                        }}/>
                </div>
              </div>
              <div className='column'>
                <div className='input-group'>
                  <Input  label='Instagram Url'
                          leftIcon='fab fa-instagram'
                          inputProps={{
                            name: 'instagram_url',
                            onChange: this.textChange,
                            value: this.state.instagram_url || '',
                        }}/>
                  <Input  label='Facebook Url'
                          leftIcon='fab fa-facebook-f'
                          inputProps={{
                            name: 'facebook_url',
                            onChange: this.textChange,
                            value: this.state.facebook_url || '',
                        }}/>
                  <Input  label='Twitter Url'
                          leftIcon='fab fa-twitter'
                          inputProps={{
                            name: 'twitter_url',
                            onChange: this.textChange,
                            value: this.state.twitter_url || '',
                        }}/>
                </div>
              </div>
            </div>
            <div className='field is-grouped'>
              <div className='control'>
                <Button className='button is-text'
                        onClick={this.navigateBack}
                        >Cancel
                </Button>
              </div>
              <div className='control'>
                <Button className='button pink-button is-link'
                        onClick={this.submit}
                        >Save
                </Button>
              </div>
            </div>
          </div>
        }
      </div>
    )
  }
}
