import React from 'react'
import StoreBusinessInfo from 'components/store/business_info'

const AdminStoreBusinessInfo = ({store}) => (
  <div className='full-width'>
    <StoreBusinessInfo store={store}/>
    {store.phone &&
      <div>
        <span className='icon color-pink'
              style={{paddingRight: '0.25rem'}}>
          <i className='fas fa-phone'/>
        </span>
        <span>{store.phone}</span>
      </div>
    }
  </div>
)

export default AdminStoreBusinessInfo
