import 'url-search-params-polyfill'
import React from 'react'
import {getAdminStores} from 'api/admin/store'
import {US} from 'store/countries_store'
import debounce from 'util/debounce'
import {replaceHistory} from 'util/history'

import {AuthorizeAdmin} from 'components/common/authorize_user'
import Loader from 'components/common/loader'
import Button from 'components/common/button'
import Pagination from 'components/common/pagination'
import AdminsStoreTable from 'components/admin/store/table'
import AdminStoreCreateModal from 'components/admin/store/create'

export default class AdminStoreIndex extends React.Component {

  constructor(props) {
		let urlParams = new URLSearchParams(props.location.search)
    super()
    this.state = {
      loading: true,
      stores: [],
      pageSize: 25,
      page: urlParams.get('page') || 1,
      query: urlParams.get('search_query') || '',
      region: urlParams.get('region') || '',
      status: urlParams.get('status') || '',
			featured: urlParams.get('featured') || '',
      orderBy: 'name',
      order: 'ASC',
      totalEntries: 0,
    }
    this.setStores = this.setStores.bind(this)
    this.onInputChange = this.onInputChange.bind(this)
		this.regionChange = this.regionChange.bind(this)
    this.queryChange = this.queryChange.bind(this)
		this.pageChange = this.pageChange.bind(this)
    this.appendNewStore=this.appendNewStore.bind(this)
    this.debouncedGetStores = debounce(this.debouncedGetStores, 500)
  }

  componentWillMount() {
    this.getStores()
  }

  debouncedGetStores() {
    this.getStores()
  }

  getStores() {
    this.setState({loading: true})
		this.syncParamsToUrlParams()
    getAdminStores({params: this.params(), success: this.setStores})
  }

	params() {
    return {
      page_size: this.state.pageSize,
      page: this.state.page,
      order_by: this.state.orderBy,
      order: this.state.order,
      search_query: this.state.query,
      region: this.state.region,
      status: this.state.status,
      featured: this.state.featured
    }
	}

  onInputChange(event) {
    let name = event.currentTarget.name
    let value = event.currentTarget.value
    this.setState({[name]: value}, this.getStores)
  }

	syncParamsToUrlParams() {
		let baseUrl = this.props.location.pathname
		replaceHistory(baseUrl, this.params())
	}

  setStores(response){
		let data = response.data
    this.setState({
			stores: data.items,
			totalEntries: data.total_entries,
			page: data.page,
			pageSize: data.page_size,
			loading: false
		})
  }

  appendNewStore(store) {
    this.setState((prevState, props) => ({
        stores: [store, ...prevState.stores]
      }))
  }

	regionChange(event){
		this.setState({region: event.target.value, loading: true},
									this.getStores)
	}

  getError(response){
    notificationsStore.addError({message: 'Error occurred loading stores'})
  }

  queryChange(event){
    this.setState({query: event.target.value},
                  this.debouncedGetStores)
  }

	pageChange(event){
		let page = event.currentTarget.dataset.page
		this.setState({page: page, loading: true},
			this.getStores)
	}



  renderTopBar(){
    return(
      <div className='admin-stores-top-bar full-width pink-background base-padding'>
        <div className='columns'>
          <div className='column is-four-fifths-tablet'>
            <div className='columns'>
              <div className='column'>
                <div className="select is-inline-block">
                  <select value={this.state.region} onChange={this.regionChange}>
                    <option value=''>
                      All States
                    </option>
                    {Object.entries(US.regions).map(
                      ([code, region]) => (
                        <option key={code} value={code}>
                          {region}
                        </option>
                      )
                    )}
                  </select>
                </div>
              </div>

              <div className='column'>
                <div className='field is-inline-block base-side-margin'>
                  <p className='control has-icons-right'>
                    <input className='input is-rounded'
                            type='text'
                            placeholder='Search Stores'
                            value={this.state.query}
                            onChange={this.queryChange}
                    />
                    <span className='icon is-small is-right'>
                      <i className='fas fa-search'></i>
                    </span>
                  </p>
                </div>
                <div className="select is-inline-block">
                  <select name='status' value={this.state.status}
                    onChange={this.onInputChange}>
                    <option value=''>
                      All
                    </option>
                    <option value='active'>Active</option>
                    <option value='hidden'>Hidden</option>
                    <option value='pending'>Pending</option>
                  </select>
                </div>
              </div>
            </div>
          </div>
          <div className='column is-hidden-mobile'></div>
          <div className='column is-narrow'>
            <AdminStoreCreateModal onSuccessCreate={this.appendNewStore} />
          </div>
        </div>
      </div>
    )
  }

  render() {
    return(
      <div className='container'>
        <AuthorizeAdmin/>
        <section className='hero'>
          <div className='hero-body'>
            <h1 className='title'>Manage Stores</h1>
          </div>
        </section>
        {this.renderTopBar()}
        {this.state.loading ?
          <Loader text='Loading Stores'/>
          :
         	<AdminsStoreTable stores={this.state.stores}
														totalEntries={this.state.totalEntries}
														page={this.state.page}
														pageChange={this.pageChange}
														pageSize={this.state.pageSize} />
        }
      </div>
    )
  }
}
