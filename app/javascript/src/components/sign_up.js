import React from 'react'
import {Redirect} from 'react-router'
import {Link} from 'react-router-dom'
import {registerUser} from 'api/registration'
import Head from 'components/common/head'
import Button from 'components/common/button'
import {Input} from 'components/common/input'
import UserStore from 'store/user_store'
import NotificationsStore from 'store/notifications_store'
import isBlank from 'util/is_blank'
import validateEmail from 'util/validate_email'

const SIGN_OUT_ERR_MSG = 'Already logged into an ' +
                         'account. Sign out to ' +
                         'create another account.'
const generateParams = ({first_name, last_name,
                          email, password,
                          password_confirmation}) => (
                          {first_name, last_name,
                            email, password,
                            password_confirmation
                          }
                        )
export default class SignUp extends React.Component {

  constructor(props, context) {
    super()
    this.state = {
      first_name: '',
      last_name: '',
      email: '',
      password: '',
      password_confirmation: '',
      alreadyLoggedIn: UserStore.isLoggedIn(),
      redirect: false,
      mode: 'input'
    }
    this.onInputChange = this.onInputChange.bind(this)
    this.submit = this.submit.bind(this)
    this.onSuccess = this.onSuccess.bind(this)
    this.onError = this.onError.bind(this)
  }

  onInputChange(event) {
    this.setState({
      [event.currentTarget.name]: event.currentTarget.value
    })
  }

  isSubmitDisabled(event) {
    return isBlank(this.state.first_name) ||
           isBlank(this.state.last_name) ||
           isBlank(this.state.email) ||
           isBlank(this.state.password) ||
           isBlank(this.state.password_confirmation) ||
           !validateEmail(this.state.email) ||
           this.state.password != this.state.password_confirmation ||
           this.state.password.length < 8 ||
           this.state.mode == 'submitting'
  }

  componentWillUnmount() {
    if(UserStore.isLoggedIn() && this.state.alreadyLoggedIn){
      NotificationsStore.addWarning({
        message: SIGN_OUT_ERR_MSG
      })
    }
  }

  submit(event) {
    event.preventDefault()
    registerUser({
      params: generateParams(this.state),
      success: this.onSuccess,
      error: this.onError,
    })
    this.setState({
      mode: 'submitting'
    })
  }

  onSuccess(response) {
    NotificationsStore.addSuccess({
      message: 'Successfully Created Account & Signed In!'
    })
    let userStoreData = Object.assign(
                         {loggedIn: true},
                         response.data)
    UserStore.setUserData(userStoreData)
    this.setState({redirect: true})
  }

  onError(error) {
    let message = error.response.data.message
    NotificationsStore.addError({
      message: message,
      duration: 10
    })
    this.setState({
      mode: 'input'
    })
  }

  params() {
    return generateParams(this.state)
  }

  render() {
    return(
      <div id='sign-up-page'
           className='dead-center-wrapper yellow-background'>
        {(UserStore.isLoggedIn() || this.state.redirect) &&
          <Redirect to = '/'/>
        }
        <Head title='Sign Up'
              description='Sign up for a Resale District account'/
        >
        <div className='sign-up-form white-background'>
          <h1 className='title has-text-centered has-text-weight-bold'>
            Sign Up
          </h1>
          <form>
            <div className='columns is-multiline'>

              <div className='column is-half'>
                <Input  label='First Name'
                        inputProps={{
                          name: 'first_name',
                          onChange: this.onInputChange,
                          placeholder: 'Jane'
                        }}/>
              </div>

              <div className='column is-half'>
                <Input label='Last Name'
                       inputProps={{
                          name: 'last_name',
                          onChange: this.onInputChange,
                          placeholder: 'Doe'
                        }}/>
              </div>

              <div className='column'>
                <Input label='Email'
                       leftIcon='fas fa-envelope'
                       inputProps={{
                          name: 'email',
                          type: 'email',
                          onChange: this.onInputChange,
                          placeholder: 'jane.doe@youremail.com'
                      }}/>
              </div>

            </div>

            <Input label='Password'
                   leftIcon='fas fa-lock'
                   helper='Minimum of 8 characters'
                 inputProps={{
                    name: 'password',
                    onChange: this.onInputChange,
                    type: 'password'
                }}/>
            <Input label='Password Confirmation'
                   leftIcon='fas fa-lock'
                   inputProps={{
                      name: 'password_confirmation',
                      onChange: this.onInputChange,
                      type: 'password'
                  }}/>

            <div className='full-width dead-center-wrapper'>
              <div>
                {this.state.mode == 'input' &&
                <Button className='button pink-button is-block'
                        disabled={this.isSubmitDisabled()}
                        style={{margin: '0 auto'}}
                        type='submit'
                        onClick={this.submit}>
                  Submit
                </Button>
                }
                {this.state.mode == 'submitting' &&
                  <Button
                    className='button pink-button is-block is-loading'
                    style={{margin: '0 auto'}}
                    disabled={true}>
                    Submit
                  </Button>
                }
                <div className='has-text-centered'
                     style={{marginTop: '0.5rem'}}>
                  <Link to='/' style={{margin: '0 0.5rem'}}>
                    Cancel
                  </Link>
                  <Link to='/login' style={{margin: '0 0.5rem'}}>
                    Login
                  </Link>
                </div>
              </div>
            </div>

          </form>

        </div>
      </div>
    )
  }

}
