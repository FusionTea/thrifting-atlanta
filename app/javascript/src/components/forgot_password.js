import React from 'react'
import {Redirect} from 'react-router'
import {Link} from 'react-router-dom'
import {beginPasswordReset} from 'api/password'
import Head from 'components/common/head'
import Button from 'components/common/button'
import {Input} from 'components/common/input'
import NotificationsStore from 'store/notifications_store'
import UserStore from 'store/user_store'
import validateEmail from 'util/validate_email'

const DESCRIPTION = `
  Begin password reset if you forgot your password to your
  Resale District account
`
export default class ForgotPassword extends React.Component {

  constructor(props) {
    super()
    this.state = {
      email: '',
      mode: 'input',
      alreadyLoggedIn: UserStore.isLoggedIn(),
      redirect: UserStore.isLoggedIn()
    }
    this.onInputChange = this.onInputChange.bind(this)
    this.submit = this.submit.bind(this)
    this.onSuccess = this.onSuccess.bind(this)
    this.onError = this.onError.bind(this)
  }

  componentWillUnmount() {
    if (this.state.redirect && this.state.alreadyLoggedIn){
      NotificationsStore.addWarning({message: 'Already logged in'})
    }
  }

  onInputChange(event) {
    this.setState({
      [event.currentTarget.name]: event.currentTarget.value
    })
  }

  submit(event) {
    event.preventDefault()
    beginPasswordReset({
      params: {email: this.state.email},
      success: this.onSuccess,
      error: this.onError
    })
    this.setState({
      mode: 'submitting'
    })
  }

  onSuccess(response) {
    this.setState({redirect: true})
    NotificationsStore.addSuccess({
      message: 'Successfully sent password reset instructions'
    })
  }

  onError(error) {
    this.setState({mode: 'input'})
    NotificationsStore.addError({
      message: error.response.data.message,
      duration: 10
    })
  }

  isSubmitDisabled() {
    return !validateEmail(this.state.email) ||
           this.state.mode == 'submitting'
  }

  submitButtonClassName() {
    if (this.state.mode == 'input'){
      return 'button pink-button is-block'
    }else{
      return 'button pink-button is-block is-loading'
    }
  }

  render() {
    return(
      <div id='forgot-password-page'
           className='dead-center-wrapper yellow-background'>
        {this.state.redirect &&
          <Redirect to='/'/>
        }
        <Head title='Forgot Password'
              description={DESCRIPTION}/>
        <div className='base-padding forgot-password-form dead-center white-background'
             style={{margin: '1.5rem 0.5rem', padding: '0.8rem'}}>
          <h1 className='title has-text-centered has-text-weight-bold'>Forgot Password</h1>
          <form>
            <Input label='Email'
                   leftIcon='fas fa-envelope'
                   inputProps={{
                     name: 'email',
                     type: 'email',
                     onChange: this.onInputChange,
                     placeholder: 'jane.doe@youremail.com'
                    }}/>
             <div className='full-width dead-center-wrapper'>
               <div>
                 <Button className={this.submitButtonClassName()}
                         disabled={this.isSubmitDisabled()}
                         style={{margin: '0 auto'}}
                         type='submit'
                         onClick={this.submit}>
                   Send Password Reset Insutrctions
                 </Button>
                 <div className='has-text-centered'
                      style={{marginTop: '0.5rem'}}>
                    <Link to='/' style={{margin: '0 0.5rem'}}>
                      Home
                    </Link>
                    <Link to='/login' style={{margin: '0 0.5rem'}}>
                      Login
                    </Link>
                  </div>
                </div>
              </div>
          </form>
        </div>
      </div>
    )
  }


}
