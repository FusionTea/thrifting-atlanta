import React from 'react'
import isBlank from 'util/is_blank.js'

const defaultType = 'text'
const defaultFieldClass = null

const controlClass = (props = {}) => {
  let controlClass = 'control'
  if (props.leftIcon) {controlClass += ' has-icons-left'}
  if (props.rightIcon) {controlClass += ' has-icons-right'}

  return controlClass
}

let fieldClass = (props = {}) => {
  let fieldClass = 'field'
  if (props.horizontal) {fieldClass += ' is-horizontal'}

  return fieldClass
}

export const Input = (props = {}) => {
  let className = 'input'
  if (props.required && isBlank(props.inputProps.value)) {
    className += ' is-danger'
  }
  return(
    <div className={fieldClass(props)}>
      {props.label &&
        <label className='label'>{props.label}</label>
      }
      <div className={controlClass(props)}>
        <input className={className}
                type={defaultType}
                {...props.inputProps}/>
        {props.leftIcon &&
          <span className='icon is-left'>
            <i className={props.leftIcon} />
          </span>
        }
        {props.rightIcon &&
          <span className='icon is-right'>
            <i className={props.rightIcon} />
          </span>
        }
      </div>
      {props.helper &&
        <p className='help color-black'>{props.helper}</p>
      }
    </div>
  )
}

export const Textarea = (props = {}) => {
  return(
    <div className={fieldClass(props)}>
      {props.label &&
        <label className='label'>{props.label}</label>
      }
      <div className={controlClass(props)}>
        <textarea className='textarea'
                  {...props.inputProps}>
          {props.inputProps.value}
        </textarea>
      </div>
    </div>
  )
}

export const Checkbox = (props = {}) => {
  return(
    <div className={fieldClass(props)}>
      <CheckboxControl {...props} />
    </div>
  )
}

export const CheckboxControl = (props = {}) => (
  <div className={controlClass(props)}>
    <label className='checkbox'>
      <input type='checkbox'
              {...props.inputProps}/>
      {props.label}
    </label>
  </div>
)

export const Select = (props = {}) => (
  <div className={fieldClass(props)}>
    <label className='label'>{props.label}</label>
    <SelectControl {...props.inputProps} />
  </div>
)

// Easy select box styling input
// expects options in the current format:
// {
//   name: value,
//   name: value,
//   name: value
// }
export const SelectControl = (props = {}) => (
  <div className={controlClass(props)}>
    <div className='select'>
      <select name={props.name} value={props.value} onChange={props.onChange} >
        {Object.entries(props.options)
                .map(([name, value]) => (
          <option key={name} value={value}>
            {name}
          </option>
        ))
        }
      </select>
    </div>
  </div>
)

export const File = (props = {}) => {
  let fileClass = 'file'
  if (props.fileName) {fileClass += ' has-name'}
  return(
    <div className={fileClass} >
      <label className='file-label'>
        <input className='file-input' type='file' {...props.inputProps} />
        <span className='file-cta'>
          <span className='file-icon'>
            <i className='fas fa-upload'/>
          </span>
          <span className='file-label'>
            {props.label || 'Choose a file'}
          </span>
        </span>
        {props.fileName &&
          <span className='file-name'>
            {props.fileName}
          </span>
        }
      </label>
    </div>
  )
}


export default Input
