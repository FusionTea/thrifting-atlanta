import React from 'react'

const defaultText = 'No Results'
const defaultIcon = 'far fa-frown fa-2x'

const NoResults = (props = {}) => (
  <div className='full-width base-padding dead-center-wrapper'>
    <div className='no-results'>
      <span className='icon has-text-center has-text-danger'>
        <i className={props.icon || defaultIcon}></i>
      </span>
      <span className='is-size-5'>
        <strong className='has-text-danger'>
          {props.text || defaultText}
        </strong>
      </span>
    </div>
  </div>
)

export default NoResults
