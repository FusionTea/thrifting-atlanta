import React from 'react'
import {Redirect} from 'react-router-dom'
import UserStore from 'store/user_store'
import NotificationsStore from 'store/notifications_store'

const defaultRedirect = '/'
const authMessage = 'You are not authorized to view that page'

const unauthroizedMessage = () => {
  NotificationsStore.addError({message: authMessage})
  return true
}

export class AuthorizeUser extends React.Component {

  constructor(props){
    super()

    this.state = {
      user: UserStore.getUser(),
			navOpen: false
    }

    this.updateUser = this.updateUser.bind(this)
  }

  componentWillMount() {
    UserStore.subscribe(this.updateUser)
  }

  componentWillUnmount() {
    UserStore.unsubscribe(this.updateUser)
  }

  updateUser(){
    this.setState({user: UserStore.getUser()})
  }

  render() {
    const redirect = !this.state.user.loggedIn
    return(
      <div>
        {redirect && unauthroizedMessage() &&
          <Redirect to={defaultRedirect} />
        }
      </div>
    )
  }
}

export class AuthorizeAdmin extends React.Component {

  constructor(props){
    super()

    this.state = {
      user: UserStore.getUser(),
			navOpen: false
    }

    this.updateUser = this.updateUser.bind(this)
  }

  componentWillMount() {
    UserStore.subscribe(this.updateUser)
  }

  componentWillUnmount() {
    UserStore.unsubscribe(this.updateUser)
  }

  updateUser(){
    this.setState({user: UserStore.getUser()})
  }

  render(){
    const redirect = !this.state.user.loggedIn || !UserStore.isAdmin()
    return(
      <div>
        {redirect && unauthroizedMessage() &&
          <Redirect to={defaultRedirect} />
        }
      </div>
    )
  }
}
