import React from 'react'

const defaultText = 'Loading...'

const Loader = (props = {}) => (
  <div className='full-width base-padding dead-center-wrapper'>
    <div className='loader-wrapper'>
			<div className='ta-loader'
           style={props.loaderStyle || {}}>
			</div>
			<div className='has-text-center is-size-5'>
				<strong style={props.textStyle || {}}>
          {props.text || defaultText}
        </strong>
			</div>
    </div>
  </div>
)

export default Loader
