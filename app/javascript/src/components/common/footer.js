import React from 'react'

const Footer = () => (
  <footer className='ta-footer footer'>
    <div className='container'>
      <div className='content has-text-centered'>
        <img className='footer-logo'
              src='/the-resale-district-logo.png'/>
        <div className='footer-social'>
          <a href='https://www.facebook.com/TheResaleDistrict/'>
            <span className='icon color-pink'>
              <i className='fab fa-facebook-f fa-2x'/>
            </span>
          </a>

          <a href='https://twitter.com/resalestyle'>
            <span className='icon color-pink'>
              <i className='fab fa-twitter fa-2x'/>
            </span>
          </a>

          <a href='https://www.instagram.com/theresaledistrict/'>
            <span className='icon color-pink'>
              <i className='fab fa-instagram fa-2x'/>
            </span>
          </a>
        </div>

        <div className='footer-copyright'>
          <span className='is-size-6 color-pink'>
            <em>&#169; The Resale District {(new Date()).getFullYear()}</em>
          </span>
        </div>
      </div>
    </div>
  </footer>
)

export default Footer
