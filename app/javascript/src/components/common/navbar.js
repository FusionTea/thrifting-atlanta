import React from 'react'
import Button from 'components/common/button'
import {Link, NavLink} from 'react-router-dom'
import UserStore from 'store/user_store'
import NotificationsStore from 'store/notifications_store'

const SUBMIT_STORE_URL = 'https://docs.google.com/forms/d/e/1FAIpQLSebW4VNsOkqjMycspxOPXjSFnEt6YfZ_oCeBNYyy-FmJbgUMw/viewform'
const BLOG_URL = 'http://blog.theresaledistrict.com/'

const NAVBAR_STYLE = {
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'space-between'
}

export default class Navbar extends React.Component {

  constructor(props){
    super()

    this.state = {
      user: UserStore.getUser(),
			navOpen: false
    }

    this.updateUser = this.updateUser.bind(this)
    this.logoutUser = this.logoutUser.bind(this)
		this.successfulLogout = this.successfulLogout.bind(this)
		this.toggleNav = this.toggleNav.bind(this)
  }

  componentWillMount() {
    UserStore.subscribe(this.updateUser)
  }

  componentWillUnmount() {
    UserStore.unsubscribe(this.updateUser)
  }

  updateUser(){
    this.setState({user: UserStore.getUser()})
  }

  logoutUser(){
    UserStore.logoutUser({
      success: this.successfulLogout,
      error: this.errorLogout
    })
  }

	successfulLogout(response){
		NotificationsStore.addSuccess({message: 'Logged out successfully'})
		this.toggleNav()
	}

	errorLogout(error){
		NotificationsStore.addError({message: 'Unable to logout'})
	}

	toggleNav(){
		this.setState((prevState, props) =>({
			navOpen: !prevState.navOpen
		}))
	}

	navClassName(){
		return (this.state.navOpen) ? 'open' : ''
	}

  render() {
    return(
      <div className='ta-navbar-container'>
        <div className='ta-navbar'>
          <div className='ta-navbar-content'>
						<div className='hamburger-container'>
							<a href="javascript:void(0)"
                 className='hamburger-menu'
                 onClick={this.toggleNav}
              >
								<div className="bar"></div>
								<div className="bar"></div>
								<div className="bar"></div>
							</a>
						</div>
            <Link to ="/" className='ta-navbar-center'>
              <span className='is-hidden-mobile'>Resale</span>
              <img className='ta-navbar-logo'
                   src='/thrifting-atlanta-navbar-logo.png'/>
              <span className='is-hidden-mobile'>District</span>
            </Link>
						<div className='invisible'/>
          </div>
        </div>
        <div className={`ta-sidebar ${this.navClassName()}`}>
          <aside className="menu" style={NAVBAR_STYLE}>
            <div className='top-menu'>
              <p className='menu-label'>Navigation</p>
              <ul className='menu-list'>
                <li>
                  <NavLink to ="/" onClick={this.toggleNav} exact={true}>
                    Home
                  </NavLink>
                </li>
                <li>
                  <NavLink to ="/stores" onClick={this.toggleNav}>
                    Stores
                  </NavLink>
                </li>
                {this.state.user.loggedIn ? (
                  <li>Hi {this.state.user.firstName}</li>
                ):(
                  <NavLink to ="/login" onClick={this.toggleNav} exact={true}>
                    <li>Login</li>
                  </NavLink>
                )}
                {!this.state.user.loggedIn &&
                  <NavLink to ="/sign_up" onClick={this.toggleNav} exact={true}>
                    <li>Sign Up</li>
                  </NavLink>
                }
              </ul>
              {this.state.user.loggedIn &&
                [
                  <p className='menu-label' key='user-menu-label'>
                    Your Account
                  </p>,
                  <ul className='menu-list' key='user-menu-list'>
                    <li>
                      <Button className='button is-danger'
                              onClick={this.logoutUser}>
                        Logout
                      </Button>
                    </li>
                  </ul>
                ]
              }
              {this.state.user.loggedIn && UserStore.isAdmin() &&
                [
                  <p className='menu-label' key='admin-menu-label'>
                    Administration
                  </p>,
                  <ul className='menu-list' key='admin-menu-list'>
                    <li>
                      <NavLink to ="/admin/stores" onClick={this.toggleNav}>
                        Store Management
                      </NavLink>
                    </li>
                  </ul>
                ]
              }
              <p className='menu-label'>
                External
              </p>
              <ul className='menu-list'>
                <li>
                  <a href={BLOG_URL}>Blog</a>
                </li>
                <li>
                  <NavLink to='/stores/new'>
                    Submit a Store
                  </NavLink>
                </li>
              </ul>
            </div>
            <div className='bottom-menu'>
            </div>
          </aside>
          <div className='shade' onClick={this.toggleNav}>
          </div>
        </div>
      </div>
    )
  }
}
