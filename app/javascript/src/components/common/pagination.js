import React from 'react'

const Page = ({page, className, pageChange}) => (
  <li>
    <a className={`pagination-link ${className}`}
        aria-label={`Go To Page ${page}`}
        data-page={page}
        href="javascript:void(0)"
        onClick={pageChange}>
      {page}
    </a>
  </li>
)

const PageEllipsis = () => (
  <li><span className='pagination-ellipsis'>&hellip;</span></li>
)

const Pagination = (props) => {
  const currentPage = parseInt(props.page)
  const firstPage = 1
  const lastPage = Math.ceil(props.totalEntries/props.pageSize)
  if (firstPage === lastPage || props.totalEntries === 0) return null
  const prevPage = currentPage - 1
  const nextPage = currentPage + 1
  const isFirstPage = currentPage === 1
  const isLastPage = currentPage === lastPage
  const renderPrev = !isFirstPage
  const renderNext = !isLastPage
  const renderFirstPage = (currentPage != firstPage) && (firstPage != prevPage)
  const renderLastPage = (currentPage != lastPage) && (lastPage != nextPage)
  const renderPrevEllipsis = ((prevPage-1) > firstPage)
  const renderNextEllipsis = (lastPage > (nextPage+1))
  return (
    <div className='ta-pagination pagination is-centered'
          style={props.style}
          role='navigation'>
      <a className='pagination-previous'
          data-page={prevPage}
          onClick={props.pageChange}
          href='javascript:void(0)'
          disabled={!renderPrev}>
        Previous
      </a>
      <a className='pagination-next'
          data-page={nextPage}
          onClick={props.pageChange}
          href='javascript:void(0)'
          disabled={!renderNext}>
        Next
      </a>
      <ul className='pagination-list'>
        {renderFirstPage && <Page pageChange={props.pageChange}
                                  page={firstPage} />}
        {renderPrevEllipsis && <PageEllipsis/>}
        {renderPrev && <Page pageChange={props.pageChange}
                                  page={prevPage}/>}
        <Page page={props.page} className='is-current'/>
        {renderNext && <Page pageChange={props.pageChange}
                                  page={nextPage}/>}
        {renderNextEllipsis && <PageEllipsis/>}
        {renderLastPage && <Page pageChange={props.pageChange}
                                  page={lastPage} />}
      </ul>
    </div>
  )
}

export default Pagination
