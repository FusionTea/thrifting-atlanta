import React from 'react'
import Button from 'components/common/button'

const Notification = (props) => {
  const notification = props.notification
  const className = `${notification.type} notification`
  return(
    <div className = {className}>
      <Button className='delete'
              onClick={props.removeNotification}
              data-id = {props.id}/>
      {notification.message}
    </div>
  )
}

export default Notification
