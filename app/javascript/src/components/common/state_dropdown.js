import React from 'react';
import {US} from 'store/countries_store'

const StateDropdown = (props) => (
  <select {...props}>
    <option value=''>
      All States
    </option>
    {Object.entries(US.regions).map(
      ([code, region]) => (
        <option key={code} value={code}>
          {code}
        </option>
      )
    )}
  </select>
)

export default StateDropdown
