import React from 'react'
import isBlank from 'util/is_blank'

const DEFAULT_DESCRIPTION = `
  One-Stop guide to discover must-visit thrift, consignment and vintage
  stores in your neighborhood or nationwide!
`
export default class Description extends React.Component {

  constructor(props){
    super()
  }

  componentWillMount(){
    let descriptionEl = document.head.querySelector("meta[name='description']")
    if (!isBlank(this.props.description)){
      descriptionEl.setAttribute('content', this.props.description)
    }else{
      descriptionEl.setAttribute('content', DEFAULT_DESCRIPTION)
    }
  }

  render(){
    return null
  }

}
