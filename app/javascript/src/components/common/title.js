import React from 'react'
import isBlank from 'util/is_blank'

const BASE_TITLE = ` | The Resale District`
const DEFAULT_TITLE = `${BASE_TITLE} App`

export default class Title extends React.Component {

  constructor(props){
    super()
  }

  componentWillMount(){
    if (!isBlank(this.props.title)){
      document.title = `${this.props.title} ${BASE_TITLE}`
    }else{
      document.title = DEFAULT_TITLE
    }
  }

  render(){
    return null
  }
}
