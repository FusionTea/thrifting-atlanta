import React from 'react'
import Title from 'components/common/title'
import Description from 'components/common/description'

const Head = ({title, description}) => (
  [
    <Title key='title' title={title}/>,
    <Description key='description' description={description}/>
  ]
)

export default Head
