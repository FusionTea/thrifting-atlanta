import React from 'react'
import NotificationsStore from 'store/notifications_store'
import Notification from 'components/common/notification'

export default class Notifications extends React.Component {

  constructor(props) {
    super()

    this.state = {
      notifications: NotificationsStore.getNotifications()
    }

    this.updateNotifications = this.updateNotifications.bind(this)
    this.removeNotification = this.removeNotification.bind(this)
  }

  componentWillMount() {
    NotificationsStore.subscribe(this.updateNotifications)
  }

  componentWillUnmount() {
    NotificationsStore.unsubscribe(this.updateNotifications)
  }

  updateNotifications(){
    this.setState({notifications: NotificationsStore.getNotifications()})
  }

  removeNotification(event){
    let id = event.currentTarget.dataset.id
    NotificationsStore.remove(id)
  }

  render() {
    return(
      <div className = 'notifications-container' >
        {Object.entries(this.state.notifications)
                .map(([id, notification]) =>(
                  <Notification notification={notification}
                                key={id}
                                id={id}
                                removeNotification={this.removeNotification}/>
                  ))
        }
      </div>
    )
  }

}
