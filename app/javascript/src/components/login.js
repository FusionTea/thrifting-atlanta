import React from 'react'
import {Redirect} from 'react-router'
import {Link} from 'react-router-dom'
import UserStore from 'store/user_store'
import Head from 'components/common/head'
import Button from 'components/common/button'
import NotificationsStore from 'store/notifications_store'

export default class Login extends React.Component {

  constructor(props) {
    super()
    this.state =  {
                    email: "",
                    password: "",
                    remember_me: false,
                    error: "",
                    redirect: UserStore.isLoggedIn(),
                    alreadyLoggedIn: UserStore.isLoggedIn()
                  }
    this.handleInputChange = this.handleInputChange.bind(this)
    this.handleCheckboxChange = this.handleCheckboxChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
    this.loginSuccess = this.loginSuccess.bind(this)
    this.emailRef = React.createRef();
  }

  componentWillUnmount() {
    if(this.state.redirect && this.state.alreadyLoggedIn){
      NotificationsStore.addWarning({message: 'Already logged in'})
    }
  }

  componentDidMount() {
    this.emailRef.current.focus()
  }

  handleInputChange(event) {
    let key = event.currentTarget.name
    let value = event.currentTarget.value
    this.setState({[key]: value})
  }

  handleCheckboxChange(event) {
    let key = event.currentTarget.name
    let value = event.currentTarget.checked
    this.setState({[key]: value})
  }

  handleSubmit(event) {
    event.preventDefault()
    if(this.isValidForSubmit()){
      let options = {
        params: this.loginParams(),
        success: this.loginSuccess,
        error: this.loginError
      }
      UserStore.loginUser(options)
    }else{
      this.setState({error: 'Enter Email & Password before submitting'})
    }
  }

  loginParams() {
    return {
            email: this.state.email,
            password: this.state.password,
            remember_me: this.state.remember_me
           }
  }

  loginSuccess(response) {
    NotificationsStore.addSuccess({message: 'Logged In Successfully!'})
    this.setState({redirect: true})
  }

  loginError(error) {
    NotificationsStore.addError({message: error.response.data.message})
  }

  isValidForSubmit() {
    return (this.state.email!='' && this.state.password!='')
  }

  render() {
    return(
      <div id='login-page' className='dead-center-wrapper yellow-background'>
        {this.state.redirect &&
          <Redirect to='/' />
        }
        <Head title='Login' description='Login into your Resale District account'/>
        <div className='login-form dead-center white-background'>
          <h1 className='title has-text-centered has-text-weight-bold'>Login</h1>
          {this.state.error &&
            <div className='error-message'>
              <span>{this.state.error}</span>
            </div>
          }
          <form>
            <div className='field'>
              <label className='label'>Email:</label>
              <div className='control'>
                <input  type="email"
                        name="email"
                        value={this.state.email}
                        ref={this.emailRef}
                        className='input'
                        onChange={this.handleInputChange} />
              </div>
            </div>
            <div className='field'>
              <label className='label'>Password:</label>
              <div className='control'>
                <input  type="password"
                        name="password"
                        value={this.state.password}
                        className='input'
                        onChange={this.handleInputChange} />
              </div>
            </div>
            <div className='field'>
              <div className='control'>
                <label className='checkbox'>
                  <input  type="checkbox"
                          name="remember_me"
                          value={this.state.remember_me}
                          onChange={this.handleCheckboxChange} />
                  Remember Me
                </label>
              </div>
            </div>
            <div className='field'>
              <div className='control dead-center-wrapper'>
                <input type='submit' value='Submit' className='button pink-background' onClick={this.handleSubmit}/>
              </div>
            </div>
            <div style={{display: 'flex', justifyContent: 'space-between', margin: '0.15rem 0.5rem'}}>
                <Link to ='/forgot_password'>Forgot Password</Link>
                <Link to='/sign_up'>Sign Up</Link>
            </div>
          </form>
        </div>
      </div>
    )
  }

}
