import 'url-search-params-polyfill'
import React from 'react'
import {US} from 'store/countries_store'
import debounce from 'util/debounce'
import {replaceHistory} from 'util/history'
import isBlank from 'util/is_blank'
import {getStores} from 'api/store'
import Head from 'components/common/head'
import Loader from 'components/common/loader'
import Button from 'components/common/button'
import StateDropdown from 'components/common/state_dropdown'
import Pagination from 'components/common/pagination'
import StoreCard from 'components/store/card'
import ResultInfo from 'components/store/result_info'
import NoResults from 'components/common/no_results'


const BASE_TITLE = 'Stores'

export default class StoreIndex extends React.Component {

  constructor(props) {
    let urlParams = new URLSearchParams(props.location.search)
    super()
    this.state = {
      loading: true,
      stores: [],
      pageSize: 25,
      page: urlParams.get('page') || 1,
      searchQuery: urlParams.get('search_query') || '',
      region: urlParams.get('region') || '',
      featured: urlParams.get('featured') || false,
      orderBy: 'name',
      order: 'ASC',
      totalEntries: 0
    }

    this.setStores = this.setStores.bind(this)
    this.switchFeatured = this.switchFeatured.bind(this)
    this.pageChange = this.pageChange.bind(this)
    this.queryChange = this.queryChange.bind(this)
    this.onStateChange = this.onStateChange.bind(this)
    this.clearSearch = this.clearSearch.bind(this)
    this.clearRegion = this.clearRegion.bind(this)
    this.debouncedGetStores = debounce(this.debouncedGetStores, 500)
  }

  title(){
    let featuredTitle = this.state.featured ? `Featured `: ''
    let searchQueryTitle = isBlank(this.state.searchQuery) ? '' : `${this.state.searchQuery} `
    let regionTitle = isBlank(this.state.region) ? '' : ` in ${US.regions[this.state.region]}`
    let title=`${featuredTitle}${searchQueryTitle}${BASE_TITLE}${regionTitle}`
    return title
  }

  description(){
    let featuredDescription = this.state.featured ? 'Featured ': ''
    let regionDescription = isBlank(this.state.region) ? 'the United States' : US.regions[this.state.region]
    let searchQueryDescription = isBlank(this.state.searchQuery) ? '' : ` that match the search term "${this.state.searchQuery}"`
    let description = `${featuredDescription}Stores across ${regionDescription}${searchQueryDescription}`
    return description
  }

  componentWillMount(){
    this.getStores()
  }

  onStateChange(event) {
    this.setState({
      loading: true,
      region: event.currentTarget.value
    }, this.getStores)
  }

  clearSearch() {
    this.setState({
      loading: true,
      searchQuery: ''
    }, this.getStores)
  }

  clearRegion() {
    this.setState({
      loading: true,
      region: ''
    }, this.getStores)
  }

  switchFeatured(event) {
    this.setState({
      featured: event.target.checked
    }, this.getStores)
  }

  queryChange(event){
    this.setState({searchQuery: event.target.value},
                  this.debouncedGetStores)
  }

  debouncedGetStores() {
    this.getStores()
  }

  getStores() {
    this.setState({loading: true})
    this.syncParamsToUrlParams()
    getStores({params: this.params(), success: this.setStores})
  }

	syncParamsToUrlParams() {
		let baseUrl = this.props.location.pathname
		replaceHistory(baseUrl, this.params())
	}

  setStores(response) {
    let data = response.data
    this.setState({
      stores: data.items,
      totalEntries: data.total_entries,
      page: data.page,
      pageSize: data.page_size,
      loading: false
    })
  }

  params() {
    return {
      page_size: this.state.pageSize,
      page: this.state.page,
      order_by: this.state.orderBy,
      order: this.state.order,
      search_query: this.state.searchQuery,
      region: this.state.region,
      featured: this.state.featured
    }
  }

	pageChange(event){
		let page = event.currentTarget.dataset.page
		this.setState({page: page, loading: true},
			this.getStores)
	}

  render(){
    return(
      <div className='container'>
        <div className='full-width pink-background base-padding'
             style={{borderRadius: '5px', margin: '1rem 0'}}>
          <div className='columns is-mobile is-multiline'>

            <div className='column is-narrow' >
              <div className='select'>
                <StateDropdown value={this.state.region}
                               name='region'
                               onChange={this.onStateChange}/>
              </div>
            </div>

            <div className='column is-narrow'>
              <div className='field is-inline-block base-side-margin'>
                <p className='control has-icons-right'>
                  <input className='input is-rounded'
                          type='text'
                          placeholder='Search Stores'
                          value={this.state.searchQuery}
                          onChange={this.queryChange}
                  />
                  <span className='icon is-small is-right'>
                    <i className='fas fa-search'></i>
                  </span>
                </p>
              </div>
            </div>
          </div>{/*columns*/}
        </div>{/*search bar*/}

        <div className='field'>
            <input type='checkbox'
                   id='featured'
                   name='featured'
                   className='switch yellow-switch'
                   checked={this.state.featured}
                   onChange={this.switchFeatured}/>
            <label htmlFor="featured">Featured</label>
        </div>
        {this.state.loading ?
          <Loader text='Loading Stores'/>
        :
        <div className='results'>
          <Head title={this.title()} description={this.description()}/>
          <ResultInfo searchQuery={this.state.searchQuery}
                      region={this.state.region}
                      clearSearch={this.clearSearch}
                      totalEntries={this.state.totalEntries}
                      clearRegion={this.clearRegion}/>
          {this.state.totalEntries == 0 &&
            <NoResults text='No Stores'/>
          }
          <div className='columns is-multiline'>
            {this.state.stores.map((store) => (
              <StoreCard key={store.id}
                         store={store}
                         className='column'/>
            ))}
          </div>
        <Pagination pageChange={this.pageChange}
                    totalEntries={this.state.totalEntries}
                    pageSize={this.state.pageSize}
                    page={this.state.page}/>
        </div>
        }
      </div>
    )
  }
}
