import React from 'react'
import {getStorePhotos, deleteStorePhoto} from 'api/store_photo'
import Loader from 'components/common/loader'
import StorePhotoThumbnail from 'components/store/photo_thumbnail'
import StoreAddPhoto from 'components/store/add_photo'
import StorePhotoModal from 'components/store/photo_modal'
export default class StorePhotoCollection extends React.Component {

  constructor(props, context) {
    super()
    this.state = {
      storePhotos: [],
      loading: true,
      addNew: false,
      showPhoto: false
    }
    this.setStorePhotos = this.setStorePhotos.bind(this)
    this.onError = this.onError.bind(this)
    this.onClickDelete = this.onClickDelete.bind(this)
    this.toggleAdd = this.toggleAdd.bind(this)
    this.closePhotoModal = this.closePhotoModal.bind(this)
    this.appendStorePhoto = this.appendStorePhoto.bind(this)
    this.showPhoto = this.showPhoto.bind(this)
  }

  componentWillMount() {
    this.fetchStorePhotos()
  }

  fetchStorePhotos() {
    getStorePhotos({
      storeId: this.props.storeId,
      success: this.setStorePhotos,
      error: this.onError
    })
  }

  setStorePhotos(response) {
    this.setState({
      loading: false,
      storePhotos: response.data
    })
  }

  onError(error) {
  }

  onClickDelete(event) {
    let id = event.currentTarget.dataset.id
    deleteStorePhoto({
      id: id
    })
    this.setState((prevState) => (
     {
        storePhotos: prevState.storePhotos.filter(sp => sp.id != id)
     }
    ))
  }

  toggleAdd(event) {
    this.setState((prevState) => ( {
      addNew: !prevState.addNew
    }))
  }

  closePhotoModal(event) {
    this.setState({
      showPhoto: false
    })
  }

  appendStorePhoto(storePhoto) {
    this.setState((prevState) => ({
      storePhotos: prevState.storePhotos.concat([storePhoto])
    }))
  }

  showPhoto(event) {
    let index = parseInt(event.currentTarget.dataset.index)
    this.setState({
      showPhoto: index
    })
  }

  render() {
    if (this.state.loading){
      return(
        <Loader text='Loading photos'/>
      )
    }else{
      return(
        <div className='full-width store-photo-thumbnail-wrapper'>
          {this.state.storePhotos.map((storePhoto, index) => (
            <StorePhotoThumbnail
                 key={storePhoto.id}
                 index={index}
                 storePhoto={storePhoto}
                 onClick={this.showPhoto}
                 deletePhoto={this.props.deletePhoto}
                 onClickDelete={this.onClickDelete}/>
          ))}
          {this.state.showPhoto !== false &&
            <StorePhotoModal index={this.state.showPhoto}
                             storePhotos={this.state.storePhotos}
                             collectionLength={this.state.storePhotos.length}
                             showPhoto={this.showPhoto}
                             close={this.closePhotoModal}/>
          }
          {this.props.addPhoto &&
            <div className='add-store-photo-thumbnail dead-center-wrapper'
                 onClick={this.toggleAdd}>
              <div style={{cursor: 'pointer'}}>
                <span className='icon'>
                  <i className='fas fa-plus'/>
                </span>
                Add Photo
              </div>
            </div>
          }
          {this.props.addPhoto && this.state.addNew &&
            <StoreAddPhoto close={this.toggleAdd}
                           onSuccess={this.appendStorePhoto}
                           storeId={this.props.storeId}/>
          }
        </div>
      )
    }
  }
}
