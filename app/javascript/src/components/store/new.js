import React from 'react'
import {Link} from 'react-router-dom'

// Api
import {createStore} from 'api/store.js'

// Util
import isBlank from 'util/is_blank.js'
import {goBack} from 'util/history'

//Store
import NotificationsStore from 'store/notifications_store'

// Componenents
import Head from 'components/common/head.js'
import {Input, Textarea, Checkbox, Select} from 'components/common/input.js'
import Button from 'components/common/button.js'
import StoreAddrressInputs from 'components/store/address_inputs.js'
import StoreTagInputs from 'components/store/tag_inputs.js'

const DESCRIPTION = 'Submit a new store that will be added to our catalog'
const INIT_STATE = {
  mode: 'edit',
  name: '',
  description: '',
  business_hour_info: '',
  email: '',
  phone: '',
  website_url: '',
  twitter_url: '',
  address: {region: 'AK'},
  tags: {},
}

export default class StoreNew extends React.Component {

  constructor(props) {
    super(props)
    this.state = INIT_STATE
    this.submit = this.submit.bind(this)
    this.textChange = this.textChange.bind(this)
    this.updateAddress = this.updateAddress.bind(this)
    this.updateTags = this.updateTags.bind(this)
    this.onSuccess = this.onSuccess.bind(this)
    this.onError = this.onError.bind(this)
    this.resetForm = this.resetForm.bind(this)
  }

  textChange(event) {
    let value = event.target.value
    let name = event.target.name
    this.setState({[name]: value})
  }

  updateAddress(address) {
    this.setState({address: address})
  }

  updateTags(tags) {
    this.setState({tags: tags})
  }

  isSubmitDisabled() {
    if (this.state.mode == 'submitting') {return true}
    let missingRequired = isBlank(this.state.name) ||
      isBlank(this.state.address.line_1) ||
      isBlank(this.state.address.municipality) ||
      isBlank(this.state.address.region) ||
      isBlank(this.state.address.postal_code)
    if (missingRequired) {return true}
    return false
  }

  submit() {
    this.setState({mode: 'submitting'})
    createStore({
      params: this.params(),
      success: this.onSuccess,
      error: this.onError
    })
  }

  onSuccess(response) {
    this.setState({mode: 'submitted'})
    NotificationsStore.addSuccess({
      message: `Successfully Submitted ${this.state.name}`
    })
  }

  onError(repsonse) {
    this.setState({
      mode: 'edit'
    })
    NotificationsStore.addError({
      message: 'Error occurred when trying to submit the store'
    })
  }

  resetForm() {
    this.setState(INIT_STATE)
  }

  params() {
    let selectedTagIds = Object.entries(this.state.tags)
      .filter(([tagId, checked]) => checked)
      .map(([tagId, checked]) => tagId)
    return {
      name: this.state.name,
      description:  this.state.description,
      business_hour_info: this.state.business_hour_info,
      email: this.state.email,
      phone: this.state.phone,
      facebook_url:  this.state.facebook_url,
      twitter_url:  this.state.twitter_url,
      instagram_url:  this.state.instagram_url,
      website_url:  this.state.website_url,
      address_attributes:  this.state.address,
      tag_ids: selectedTagIds
    }
  }

  render() {
    return(
      <div className='container'>
        <Head title='New Store Submission'
          description={DESCRIPTION}
        />
        <section className='hero'>
          <div className='hero-body'>
            <h1 className='title'>Submit New Store</h1>
            <h2 className='subtitle'>
              You can submit a store to be displayed on The Resale District.
              All fields highlighted in red are required.
            </h2>
          </div>
        </section>
        {(this.state.mode == 'edit' || this.state.mode == 'submitting') &&
          <div className='new-store-submission'>

            <div className='columns'>
              <div className='column'>
                <Input  label='Name'
                  required={true}
                  inputProps={{
                    name: 'name',
                    onChange: this.textChange,
                    value: this.state.name || '',
                    placeholder: 'Store Name'
                  }}
                />
              </div>
              <div className='column'>
                <Input  label='Email'
                  inputProps={{
                    name: 'email',
                    onChange: this.textChange,
                    value: this.state.email || '',
                    placeholder: 'store@email.com'
                  }}
                />
              </div>
              <div className='column'>
                <Input  label='Phone'
                  leftIcon='fas fa-phone'
                  inputProps={{
                    name: 'phone',
                    onChange: this.textChange,
                    value: this.state.phone || '',
                    placeholder: '312 123-1234'
                  }}
                />
              </div>
            </div>

            <StoreAddrressInputs
              address={this.state.address}
              updateAddress={this.updateAddress}
            />

            <StoreTagInputs tags={this.state.tags}
              updateTags={this.updateTags}
            />

            <div className='columns'>
              <div className='column is-two-thirds'>
                <Textarea label='Description'
                          inputProps={{
                            name: 'description',
                            onChange: this.textChange,
                            value: this.state.description || '',
                            placeholder: 'Description Of Store'
                          }}/>
              </div>
              <div className='column'>
                <Textarea label='Business Hours Info'
                          inputProps={{
                            name: 'business_hour_info',
                            onChange: this.textChange,
                            value: this.state.business_hour_info || '',
                            placeholder: 'Monday-Friday: 8:00 AM - 5:00 PM\nSaturday-Sunday: Closed'
                          }}/>
                </div>
            </div>

            <div className='new-store-urls input-group'>
              <h2 className='subtitle'>URLS</h2>
              <div className='columns is-multiline'>

                <div className='column'>
                  <Input  label='Website Url'
                    leftIcon='far fa-globe'
                    inputProps={{
                      name: 'website_url',
                      onChange: this.textChange,
                      value: this.state.website_url || '',
                      placeholder: 'www.yourstore.com'
                    }}
                  />
                </div>

                <div className='column'>
                  <Input  label='Instagram Url'
                          leftIcon='fab fa-instagram'
                          inputProps={{
                            name: 'instagram_url',
                            onChange: this.textChange,
                            value: this.state.instagram_url || '',
                        }}/>
                </div>

                <div className='column'>
                  <Input  label='Facebook Url'
                          leftIcon='fab fa-facebook-f'
                          inputProps={{
                            name: 'facebook_url',
                            onChange: this.textChange,
                            value: this.state.facebook_url || '',
                        }}/>
                </div>

                <div className='column'>
                  <Input  label='Twitter Url'
                          leftIcon='fab fa-twitter'
                          inputProps={{
                            name: 'twitter_url',
                            onChange: this.textChange,
                            value: this.state.twitter_url || '',
                        }}/>
                </div>

              </div>
            </div>

            <div className='field is-grouped save-buttons-container'>
              <div className='control'>
                <Button className='button is-text'
                        onClick={goBack}
                        >Cancel
                </Button>
              </div>
              <div className='control'>
                <Button className='button pink-button is-link'
                        onClick={this.submit}
                        disabled={this.isSubmitDisabled()}
                        >Save
                </Button>
              </div>
            </div>

          </div>
        }
        {this.state.mode == 'submitted' &&
          <div>
            <p>
              Congratulations. <strong>You successfuly submitted{' '}
              {this.state.name}!</strong> The store is currently pending{' '}
              review. If you would like to submit another store.{' '}
              Click the 'Submit New' button below.
            </p>
            <div className='full-width dead-center-wrapper'
              style={{marginTop: '1.5rem'}}>
              <div className='buttons-wrapper'>
                <Link to='/' className='button is-link'>
                  Go Home
                </Link>
                <Button className='button pink-button is-link'
                  style={{marginLeft: '0.25rem'}}
                  onClick={this.resetForm}>
                  Submit New
                </Button>
              </div>
            </div>
          </div>
        }
      </div>
    )
  }
}
