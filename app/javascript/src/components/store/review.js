import React from 'react'
import {DateTime} from 'luxon'

const reviewName = (review) => {
  if (review.user != null){
    return `${review.user.first_name} ${review.user.last_name}`
  }else{
    return review.reviewer_name
  }
}

const reviewDate = (date) => (
  DateTime.fromISO(date).toFormat('LLLL d y')
)

const StoreReview = ({review}) => (
  <div className='store-review'>
    <div className='store-review-header'>
      <span className='store-review-name'>
        {reviewName(review)}
      </span>
      <span className='store-review-date'>
        {reviewDate(review.date)}
      </span>
    </div>
    <div className='store-review-content'>
      {review.content}
    </div>
  </div>
)

export default StoreReview
