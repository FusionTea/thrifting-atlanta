import React from 'react'
import Button from 'components/common/button'

const thumbnailUrl = (id) => (
  `/api/store_photos/${id}/thumbnail`
)

const StorePhotoThumbnail = (props) => (
  <div className='store-photo-thumbnail'>
    <figure>
      <img className='thumbnail-image'
           onClick={props.onClick}
           data-index={props.index}
           data-id={props.storePhoto.id}
           alt={props.storePhoto.description}
           src={thumbnailUrl(props.storePhoto.id)}/>
    </figure>
    {props.deletePhoto &&
      <span onClick={props.onClickDelete}
              data-id={props.storePhoto.id}
              className='thumbnail-delete-icon'>
        <i className='fas fa-trash-alt'/>
      </span>
    }
  </div>
)

export default StorePhotoThumbnail
