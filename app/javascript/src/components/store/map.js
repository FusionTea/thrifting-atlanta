import React from 'react'
import {
        withScriptjs,
        withGoogleMap,
        GoogleMap,
        Marker,
        DirectionsRenderer
      } from 'react-google-maps'
import {InfoBox} from 'react-google-maps/lib/components/addons/InfoBox'

import googleMapURL from 'api/google_maps'
import Loader from 'components/common/loader'
import googleMapURLGenerator from 'util/google_map_url_generator'

const ContainerElement = () => (
  <div style={{height: '300px', width: '100%'}}/>
)

const LoaderElement = () => (
  <div className='full-size dead-center-wrapper'>
    <Loader text='Loading Map'/>
  </div>
)

const MyMapComponent = withScriptjs(withGoogleMap((props) => (
  <GoogleMap
    onClick={props.onMapClick}
    defaultZoom={14}
    center={props.markerPosition}>
    <Marker position={props.markerPosition}/>
    <InfoBox
      defaultPosition={new google.maps.LatLng(props.markerPosition)}
      options={{closeBoxURL: ``, enableEventPropagation: true}}>
      <div className='store-map-info-box'>
        <a href={googleMapURLGenerator(props.store.address)}>
          <div className='store-map-info-box-navgiation'>
            <strong>{props.store.name}</strong>
          </div>
          <div className='store-map-info-box-address'>
            {props.store.address.line_1}, {' '}
            {props.store.address.municipality}, {' '}
            {props.store.address.region}, {' '}
            {props.store.address.postal_code}
          </div>
        </a>
      </div>
    </InfoBox>
  </GoogleMap>
)))

const StoreMap = ({store, loading}) => (
  <div className='full-width'>
    {loading ?
      <LoaderElement />
      :
      <MyMapComponent
        markerPosition={{lat: Number(store.latitude), lng: Number(store.longitude)}}
        googleMapURL={googleMapURL}
        loadingElement={LoaderElement()}
        containerElement={ContainerElement()}
        store={store}
        mapElement={<div style={{height: '100%'}}/>}
      />
    }
  </div>
)

export default StoreMap
