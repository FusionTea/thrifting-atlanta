import React from 'react'

const StoreWeblinks = ({store}) => (
  <div className='full-width'>
    {store.website_url &&
      <div>
        <a href={store.website_url}
           className='store-info-url'>
        <span className='icon color-pink'
              style={{paddingRight: '0.25rem'}}>
          <i className='fas fa-globe'/>
        </span>
        <span>{store.website_url}</span>
        </a>
      </div>
    }
    {store.twitter_url &&
      <div>
        <a href={store.twitter_url}
           className='store-info-url'>
        <span className='icon color-pink'
              style={{paddingRight: '0.25rem'}}>
          <i className='fab fa-twitter'/>
        </span>
        <span>{store.twitter_url}</span>
        </a>
      </div>
    }
    {store.facebook_url &&
     <div>
      <a href={store.facebook_url}
         className='store-info-url'>
        <span className='icon color-pink'
              style={{paddingRight: '0.25rem'}}>
          <i className='fab fa-facebook-f'/>
        </span>
        <span>{store.facebook_url}</span>
        </a>
      </div>
    }
    {store.instagram_url &&
     <div>
        <a href={store.instagram_url}
           className='store-info-url'>
        <span className='icon color-pink'
              style={{paddingRight: '0.25rem'}}>
          <i className='fab fa-instagram'/>
        </span>
        <span>{store.instagram_url}</span>
        </a>
      </div>
    }
  </div>
)

export default StoreWeblinks
