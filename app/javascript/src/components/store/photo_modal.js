import React from 'react'

const imageUrl = (id) => (
  `/api/store_photos/${id}`
)

const showLeftCaret = (props) => {
  return props.index != 0
}

const showRightCaret = (props) => {
  return props.index != (props.storePhotos.length - 1)
}

const StorePhotoModal = (props) => {
  let storePhoto=props.storePhotos[props.index]

  let leftPhoto = null
  let showLeft = showLeftCaret(props)
  if (showLeft) {leftPhoto = props.storePhotos[props.index-1]}

  let rightPhoto = null
  let showRight = showRightCaret(props)
  if (showRight) {rightPhoto = props.storePhotos[props.index+1]}

  return(
    <div className='modal is-active'>
      <div className='modal-background'
           onClick={props.close}/>
      <div className='modal-content'>
        <div className='store-photo-modal-wrapper full-size'>

          {showLeftCaret(props) ?
            <div className='dead-center-wrapper store-photo-button-icon'
                 data-index={props.index - 1}
                 onClick={props.showPhoto}>
              <span className='icon'>
                {showLeft &&
                  <i className='fas fa-4x fa-angle-left'/>
                }
                {showLeft &&
                  <img src={imageUrl(leftPhoto.id)} style={{display: 'none'}}/>
                }
              </span>
            </div>
            :
            <div className='store-photo-button-icon-filler'/>
          }

          <div>
            <img src={imageUrl(storePhoto.id)}
                 alt={storePhoto.description}/>
          </div>

          {showRightCaret(props) ?
            <div className='dead-center-wrapper store-photo-button-icon'
                  data-index={props.index + 1}
                  onClick={props.showPhoto}>
              <span className='icon'>
                {showRightCaret(props) &&
                  <i className='fas fa-4x fa-angle-right'
                      data-index={props.index + 1}
                      onClick={props.showPhoto}/>
                }
                {showRight &&
                  <img src={imageUrl(rightPhoto.id)} style={{display: 'none'}}/>
                }
              </span>
            </div>
          :
            <div className='store-photo-button-icon-filler'/>
          }

        </div>
      </div>
      <button className='modal-close is-large'
              aria-label='close'
              onClick={props.close}/>
    </div>
 )
}

export default StorePhotoModal
