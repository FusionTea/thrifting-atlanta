import React from 'react'
import {createStorePhoto} from 'api/store_photo'
import Button from 'components/common/button'
import {Input,File} from 'components/common/input'
import Loader from 'components/common/loader'

const imagePreviewStyle = {
  height: 250,
  margin: '0.5rem auto',
  width: '100%'
}

export default class StoreAddPhoto extends React.Component {

  constructor(props,context) {
    super()
    this.state = {
      mode: 'input',
      description: '',
      image: null,
      image_filename: null,
      preview_url: null
    }

    this.onFileChange = this.onFileChange.bind(this)
    this.onInputChange = this.onInputChange.bind(this)
    this.submit = this.submit.bind(this)
    this.onCreateSuccess = this.onCreateSuccess.bind(this)
    this.onCreateError = this.onCreateError.bind(this)
  }

  onFileChange(event) {
    event.preventDefault()
    let reader = new FileReader()
    let file = event.target.files[0]
    let image_filename = file.name
    reader.onloadend = () => {
      this.setState({
        image: file,
        image_filename: image_filename,
        preview_url: reader.result
      })
    }

    reader.readAsDataURL(file)
  }

  onInputChange(event) {
    this.setState({
      [event.currentTarget.name]: event.currentTarget.value
    })
  }

  submit(event) {
    this.setState({mode: 'submitting'})
    let formData = new FormData()
    formData.append('image', this.state.image)
    formData.append('description', this.state.description)
    formData.append('store_id', this.props.storeId)
    createStorePhoto({
      params: formData,
      success: this.onCreateSuccess,
      error: this.onCreateError
    })
  }

  onCreateSuccess(response) {
    this.props.onSuccess(response.data)
    this.props.close()
  }

  onCreateError(error) {
    this.setState({
      mode: 'input'
    })
  }

  isDisabled(event) {
    return this.state.description.trim() == '' ||
            this.state.image == null ||
            this.state.mode == 'submitting'
  }

  render() {
    return(
      <div className='modal is-active'>
        <div className='modal-background'
             onClick={this.props.close}/>
        <div className='modal-card'>
          <header className='modal-card-head'>
            <p className='modal-card-title'>Add Photo</p>
            <Button className='delete'
                    aria-label='close'
                    onClick={this.props.close}/>
          </header>
          <section className='modal-card-body'>
            {this.state.mode == 'input' &&
              <div>
              <File label='Upload Photo'
                    fileName={this.state.image_filename}
                    inputProps={{
                      onChange: this.onFileChange
                    }}/>
              <div className='image-preview-wrapper'
                   style={imagePreviewStyle}>
                {this.state.preview_url ?
                  <img src={this.state.preview_url}
                       style={{maxHeight: 250}}
                       alt="Cover Image Preview.."/>
                  :
                  <div className='full-size dead-center-wrapper'>
                    Image Preview Box
                  </div>
                }
              </div>
              <Input label='Description'
                     inputProps={{
                        name: 'description',
                        placeholder: 'description of image',
                        onChange: this.onInputChange
                     }}/>
              </div>
            }
            {this.state.mode == 'submitting' &&
              <Loader text='Uploading Photo...'/>
            }
          </section>
          <footer className='modal-card-foot'>
            <div className='field is-grouped'>
              <Button className='button is-text'
                      onClick={this.props.close}>
                Cancel
              </Button>
              <Button disabled={this.isDisabled()}
                      className='button pink-button is-link'
                      onClick={this.submit}>
                Add
              </Button>
            </div>
          </footer>
        </div>
      </div>
    )
  }

}
