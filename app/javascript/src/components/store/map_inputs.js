import React from 'react'
import {withScriptjs, withGoogleMap, GoogleMap, Marker} from 'react-google-maps'
import SearchBox from 'react-google-maps/lib/components/places/SearchBox'
import googleMapURL from 'api/google_maps'
import Loader from 'components/common/loader'

const ContainerElement = () => (
  <div style={{height: '300px', width: '100%'}}/>
)

const LoaderElement = () => (
  <div className='full-size dead-center-wrapper'>
    <Loader text='Loading Map'/>
  </div>
)

const defaultCenter = {
  lat: 33.780408,
  lng: -84.382377
}

const setCoordinates = (cordinates) => {
  if (cordinates == null || cordinates.lat == 0 || cordinates.lng == 0){
    return defaultCenter
  }else{
    return cordinates
  }
}

const defaultZoom = 16

const MapComponent = withScriptjs(withGoogleMap((props) => (
  <GoogleMap
		ref={props.setMapRef}
    onClick={props.onMapClick}
    defaultZoom={props.defaultZoom || defaultZoom}
    defaultCenter={setCoordinates(props.markerPosition)}
    center={setCoordinates(props.center)}>
    {props.isMarkerShown &&
      <Marker position={setCoordinates(props.markerPosition)}/>
    }

    <SearchBox
      ref={props.setSearchBoxRef}
      bounds={props.bounds}
      controlPosition={google.maps.ControlPosition.TOP_LEFT}
      onPlacesChanged={props.onPlacesChanged}>

      <input
        type='text'
        placeholder='Search for location'
        style={{
          boxSizing: 'border-box',
          border: '1px solid transparent',
          width: '20rem',
          height: '2rem',
          marginTop: '1rem',
          padding: '0 0.75rem',
          borderRadius: '0.25rem',
          boxShadow: '0 0.125rem 0.25rem rgba(0,0,0,0.3)',
          fontSize: '0.8rem',
          outline: 'none',
          textOverflow: 'ellipses'
        }}
      />
    </SearchBox>
  </GoogleMap>
)))

export default class StoreMapInputs extends React.Component {

		constructor(props) {
			super(props)

      this.state = {
        center: props.markerPosition,
      }

			this.setMapRef = this.setMapRef.bind(this)
			this.setSearchBoxRef = this.setSearchBoxRef.bind(this)
      this.onPlacesChanged = this.onPlacesChanged.bind(this)
		}

    setMapRef(element) {
      this.map = element
    }

		setSearchBoxRef(element) {
      this.searchBox = element
		}

    onPlacesChanged(event) {
      const places = this.searchBox.getPlaces()
      const bounds = new google.maps.LatLngBounds()

      places.forEach((place) => {
        if (place.geometry.viewport) {
          bounds.union(place.geometry.viewport)
        } else {
          bounds.extend(place.geometry.location)
        }
      })

      const nextMarkers = places.map((place) => (
        {
          position: place.geometry.location
        }
      ))

      let newPosition = nextMarkers[0] && nextMarkers[0].position
      this.setState({
        center: newPosition
      })
      this.props.updateCoordinates(newPosition)
    }

    render() {
			return(<MapComponent googleMapURL={googleMapURL}
                  loadingElement={LoaderElement()}
                  containerElement={ContainerElement()}
                  isMarkerShown={true}
                  onPlacesChanged={this.onPlacesChanged}
                  setMapRef={this.setMapRef}
                  onPlacesChanged={this.onPlacesChanged}
                  setSearchBoxRef={this.setSearchBoxRef}
                  mapElement={<div style={{height: '100%'}}/>}
                  center={this.state.center}
                  {...this.props} />)
    }
}
