import React from 'react'
import {createStoreReview} from 'api/store_review'
import Button from 'components/common/button'
import {Input, Textarea} from 'components/common/input'
import UserStore from 'store/user_store'
import Loader from 'components/common/loader'
import validateEmail from 'util/validate_email'

export default class StoreCreateReview extends React.Component {

  constructor(props, context){
    super()
    this.state = {
      content: '',
      reviewer_name: '',
      reviewer_email: '',
      mode: 'input',
      user: null
    }

    this.onInputChange = this.onInputChange.bind(this)
    this.submit = this.submit.bind(this)
    this.onSuccess = this.onSuccess.bind(this)
    this.onError = this.onError.bind(this)
  }

  componentWillMount() {
    UserStore.subscribe(this.updateUser)
  }

  componentWillUnmount() {
    UserStore.unsubscribe(this.updateUser)
  }

  updateUser(){
    this.setState({user: UserStore.getUser()})
  }

  onInputChange(event) {
    this.setState({
      [event.currentTarget.name]: event.currentTarget.value
    })
  }

  submit(event) {
    this.setState({mode: 'submitting'})
    createStoreReview({
      storeId: this.props.storeId,
      params: this.params(),
      success: this.onSuccess,
      error: this.onError
    })
  }

  params() {
    return {
      content: this.state.content,
      reviewer_name: this.state.reviewer_name,
      reviewer_email: String(this.state.reviewer_email).toLowerCase
    }
  }

  onSuccess(response) {
    this.setState({mode: 'submitted'})
  }

  onError(response) {
    this.setState({mode: 'input'})
    NotificationsStore.addError({
      message: 'Uh-oh, something went wrong trying to submit your review'
    })
  }

  isDisabled() {
    if (UserStore.isLoggedIn()){
      return this.state.content == ''
    }else{
      return this.state.content == '' ||
              this.state.reviewer_name == '' ||
              !validateEmail(this.state.reviewer_email)
    }
  }

  inputMode() {
    return(
      <div>
        <Textarea inputProps={{
                    name: 'content',
                    value: this.state.content,
                    onChange: this.onInputChange,
                    placeholder: 'Leave a review',
                    rows: 2
                  }}/>
        {UserStore.isLoggedIn() ?
          this.submitButton()
        :
          <div className='columns'>
            <div className='column is-narrow'>
              <Input leftIcon='fas fa-user'
                     inputProps={{
                        name: 'reviewer_name',
                        value: this.state.reviewer_name,
                        onChange: this.onInputChange,
                        placeholder: 'Jane Doe'
                    }}/>
            </div>
            <div className='column is-narrow'>
              <Input inputProps={{
                        name: 'reviewer_email',
                        state: this.state.reviewer_email,
                        onChange: this.onInputChange,
                        placeholder: 'jane.doe@gmail.com',
                      }}
                     leftIcon='fas fa-envelope'/>
            </div>
            <div className='column'>
              {this.submitButton()}
            </div>
          </div>

        }
      </div>
    )
  }

  submitButton() {
    return(
      <div className='full-width flex-right-wrapper'>
        <Button className='button pink-button'
                disabled={this.isDisabled()}
                onClick={this.submit}>
          Submit
        </Button>
      </div>
    )
  }

  render() {
    return(
      <div className='full-width base-top-bottom-margin'>
        {this.state.mode == 'input' &&
          this.inputMode()
        }
        {this.state.mode == 'submitting' &&
          <Loader text='Submitting Review'/>
        }
        {this.state.mode == 'submitted' &&
          <span>Successfully submitted your review!</span>
        }
      </div>
    )
  }
}
