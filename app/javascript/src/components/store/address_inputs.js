import React from 'react'
import {US} from 'store/countries_store'
import {Select, Input} from 'components/common/input'

export default class StoreAddrressInputs extends React.Component {

  constructor(props) {
    super()
    this.state = props.address
    this.updateField = this.updateField.bind(this)
  }

  updateField(event) {
    let key = event.currentTarget.name
    let value = event.currentTarget.value
    this.setState({[key]: value}, this.updateAddress)
  }

  updateAddress() {
    this.props.updateAddress(this.state)
  }

  regionOptions() {
    let options = {}
    Object.entries(US.regions)
          .forEach(([code, name]) => (
            options[code]=code
          ))
    return options
  }

  render() {
    return(
      <div className='address-inputs input-group'>
        <h2 className='subtitle'>Address</h2>
        <Input label='Line 1'
              required={true}
              inputProps={{
                name: 'line_1',
                onChange: this.updateField,
                value: this.state.line_1 || '',
                placeholder: '814 South Bell Ave'
              }}/>
        <Input label='Line 2'
                inputProps={{
                  name: 'line_2',
                  onChange: this.updateField,
                  value: this.state.line_2 || '',
                  placeholder: 'Floor 2'
                }}/>
        <div className='columns is-mobile'>
          <div className='column'>
            <Input label='City'
                  required={true}
                  inputProps={{
                    name: 'municipality',
                    onChange: this.updateField,
                    value: this.state.municipality || '',
                    placeholder: 'Chicago'
                  }}/>
          </div>
          <div className='column is-narrow'>
            <Select label='State'
                  inputProps={{
                    name: 'region',
                    onChange: this.updateField,
                    value: this.state.region || '',
                    options: this.regionOptions()
                  }}/>
          </div>
          <div className='column'>
            <Input label='Zip'
                    required={true}
                    inputProps={{
                      name: 'postal_code',
                      onChange: this.updateField,
                      value: this.state.postal_code || '',
                      placeholder: '60612'
                    }}/>
          </div>
        </div>
      </div>
    )
  }
}
