import React from 'react'
import {US} from 'store/countries_store'
import Button from 'components/common/button'

const ClearButton = ({onClick}) => (
  <Button className='button is-danger is-small is-rounded is-outlined'
          onClick={onClick}>
    Clear
  </Button>
)

const ResultInfoTitle = ({totalEntries}) => (
  <div className='results-info-title'>
    {totalEntries == 0 &&
    <span>No stores found</span>
    }
    {totalEntries > 0 &&
    <span>Showing {totalEntries} Stores</span>
    }
  </div>
)

const ResultInfo = (props) => (
  <div className='results-info'>
    <h1 className='result-info-title'>
      <ResultInfoTitle totalEntries={props.totalEntries} />
    </h1>
    {props.searchQuery != '' &&
      <div className='result-info-entry'>
        for <em>"{props.searchQuery}"</em>
        <ClearButton onClick={props.clearSearch}/>
      </div>
    }
    {props.region != '' &&
      <div className='result-info-entry'>
        in <em>"{US.regions[props.region]}"</em>
        <ClearButton onClick={props.clearRegion}/>
      </div>
    }
  </div>
)

export default ResultInfo
