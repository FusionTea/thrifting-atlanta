import React from 'react'
import {Link} from 'react-router-dom'
import StoreTag from 'components/store/tag'

const BKGRND_STYLE = `no-repeat center center`
const LinkStyle = {
  cursor: 'pointer'
}

const tagWrapperStyle = {
  display: 'flex',
  flexWrap: 'wrap-reverse',
  height: '100%',
  alignContent: 'flex-start',
  paddingBottom: '0.5rem'
}

const StoreCard = ({store, className}) => {
  const coverImageUrl = `/api/stores/${store.id}/cover_image`
  const coverImageStyle = {
    background: `url(${coverImageUrl}) ${BKGRND_STYLE}`,
    backgroundSize: 'cover'
  }
  const address = `${store.address.municipality}, ${store.address.region}`
  return(
    <Link to={`/stores/${store.code}`}
          style={LinkStyle}
          className={className}>
      <div className='card store-card'>
        <div className='store-card-cover-image'
             style={coverImageStyle}>
          <div style={tagWrapperStyle}>
              {store.tags.slice(0,3).map((tag) =>(
                <StoreTag key={tag.id} tag={tag} tagSize='is-small' />
              ))}
          </div>
        </div>
        <div className='card-content'>
          <div className='store-card-name color-pink'>
            {store.name}
          </div>
          <div className='store-card-footer columns is-multiline is-mobile'>
            <div className='column is-narrow store-card-location'>
              <i className='fas fa-map-marker-alt'
                  style={{paddingRight: '4px'}}/>
              {address}
            </div>
            <div className='column filler-column'>
            </div>
            <div className='column is-narrow store-card-review-count'>
              {store.reviews_count} Review{store.reviews_count != 1 && 's'}
            </div>
          </div>
        </div>
      </div>
    </Link>
  )
}

export default StoreCard
