import React from 'react'
import StoreBusinessInfo from 'components/store/business_info'
import StoreWeblinks from 'components/store/weblinks'
import StoreMap from 'components/store/map'
import StoreReviews from 'components/store/reviews'
import StorePhotoCollection from 'components/store/photo_collection'

const infoWrapperStyle = {
  position: 'relative',
  marginTop: '-2rem'
}

const StoreInfo = ({store, loading, storeId})=> (
  <div className='container'
       style={infoWrapperStyle}>
    <div className='full-size dead-center-wrapper'>
      <div id='store-info'
           className='white-background full-width'>

        <StoreBusinessInfo store={store}/>

        <section className='store-info-section'>
          <h2 className='store-info-section-title'>
            Location
          </h2>
          <div className='store-info-section-content'>
            <StoreMap store={store} loading={loading}/>
          </div>
        </section>

        <section className='store-info-section'>
          <h2 className='store-info-section-title'>
            Photos
          </h2>
          <div className='store-info-section-content'>
            <StorePhotoCollection storeId={storeId}/>
          </div>
        </section>

        <section className='store-info-section'>
          <h2 className='store-info-section-title'>
            Reviews
          </h2>
          <div className='store-info-section-content'>
            <StoreReviews storeId={storeId}/>
          </div>
        </section>
      </div>
    </div>
  </div>
)

export default StoreInfo
