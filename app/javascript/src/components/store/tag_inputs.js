import React from 'react'
import TagsStore from 'store/tags_store.js'
import {CheckboxControl} from 'components/common/input'

export default class StoreTagInputs extends React.Component {

  constructor(props) {
    super()
    let tagsState = {}
    Object.entries(TagsStore.getTags()).forEach(([tagId, tagData]) => {
      tagsState[tagId] = props.tags[tagId] || false
    })
    this.state = tagsState
    this.updateTag = this.updateTag.bind(this)
  }

  updateTag(event) {
    let key = event.currentTarget.name
    let value = event.currentTarget.checked
    this.setState({[key]: value}, this.updateTags)
  }

  updateTags() {
    this.props.updateTags(this.state)
  }

  tagName(tagId) {
    let tagData = TagsStore.tagData(tagId)
    return tagData.name
  }

  render() {
    let tags = Object.entries(this.state)
    return(
      <div className='tag-inputs input-group'>
        <h2 className='subtitle'>Tags</h2>
        <div className='field is-grouped is-grouped-multiline'>
          {tags.map(([id, checked]) => (
            <CheckboxControl key={id}
                      label={this.tagName(id)}
                      inputProps={{
                        name: id,
                        onChange: this.updateTag,
                        checked: checked
                      }}/>
          ))}
        </div>
      </div>
    )
  }
}
