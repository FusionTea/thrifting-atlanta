import React from 'react'
import StoreWeblinks from 'components/store/weblinks'

const StoreBusinessInfo = ({store}) => (
  <div className='full-width'>
    <section className='store-info-section'>
      <h2 className='store-info-section-title'>
        Description
      </h2>
      <div className='store-info-section-content'>
        <p style={{whiteSpace: 'pre-wrap'}}>
          {store.description}
        </p>
      </div>
    </section>

    <section className='store-info-section'>
      <h2 className='store-info-section-title'>
        Business Information
      </h2>
      <div className='columns'>
        <div className='column'>
          <p style={{whiteSpace: 'pre-wrap'}}>
            {store.business_hour_info}
          </p>
          {store.email &&
            <a href={`mailto:${store.email}`}>
              <span className='icon color-pink'
                    style={{paddingRight: '0.25rem'}}>
                <i className='fas fa-envelope'/>
              </span>
              <span>{store.email}</span>
            </a>
          }
        </div>
        <div className='column'>
          <StoreWeblinks store={store}/>
        </div>
      </div>
    </section>
  </div>
)

export default StoreBusinessInfo
