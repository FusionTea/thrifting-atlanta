import React from 'react'
const STYLE = {
  margin: '0.125rem 0.5rem'
}

const StoreTag = ({tag, tagSize}) => (
  <span className={`tag is-yellow ${tagSize || 'is-medium'} is-rounded`}
        style={STYLE}>
    {tag.name}
  </span>
)

export default StoreTag
