import React from 'react'
import {Redirect} from 'react-router'
import {getStoreByCode} from 'api/store'
import Head from 'components/common/head'
import Loader from 'components/common/loader'
import Button from 'components/common/button'
import StoreBanner from 'components/store/banner'
import StoreInfo from 'components/store/info'
import NotificationsStore from 'store/notifications_store'

export default class StoreShow extends React.Component {

  constructor(props) {
    super()
    this.state = {
      loading: true,
      store: {},
      error: false
    }
  }

  componentDidMount(){
    let code = this.props.match.params.code
    getStoreByCode({
     code: code,
      success: (response) => {
        let store = response.data
        if (store) {
          this.setState({
              store: store,
              loading: false
          })
        }else{
          this.getStoreError()
        }
      },
      error: (response) => {
        this.getStoreError()
      }
    })
  }

  getStoreError(){
    this.setState({
      error: true
    })
    NotificationsStore.addError({message: 'Could not find store'})
  }

  render(){
    return(
      <div>
        {!this.state.loading &&
          <Head title={`${this.state.store.name} Store`}
                description={`Get reviews, photos, directions and business hours for ${this.state.store.name}`}/>
        }
        {this.state.error &&
          <Redirect to ='/'/>
        }
        {this.state.loading ?
          <Loader text='Loading store'/>
        :
        <React.Fragment>
          <StoreBanner store={this.state.store}
                       loading={this.state.loading}/>
          <StoreInfo store={this.state.store}
                     storeId={this.state.store.id}
                     loading={this.state.loading}/>
        </React.Fragment>
        }
      </div>
    )
  }
}

