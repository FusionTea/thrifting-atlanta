import React from 'react'
import {getStoreReviews} from 'api/store_review'
import {SelectControl} from 'components/common/input'
import Loader from 'components/common/loader'
import Pagination from 'components/common/pagination'
import StoreReview from 'components/store/review'
import StoreCreateReview from 'components/store/create_review'

const OrderOptions = {
  'Most Recent': 'DESC',
  'Oldest First': 'ASC'
}

export default class StoreReviews extends React.Component {

  constructor(props, context){
    super()
    this.state = {
      loading: true,
      reviews: [],
      pageSize: 10,
      page: 1,
      orderBy: 'date',
      order: 'DESC',
      totalEntries: 0,
    }
    this.setReviews = this.setReviews.bind(this)
    this.onOrderChange = this.onOrderChange.bind(this)
    this.onPageChange = this.onPageChange.bind(this)
  }

  componentWillMount() {
    this.fetchReviews()
  }

  fetchReviews(){
    this.setState({loading: true})
    getStoreReviews({
      storeId: this.props.storeId,
      params: this.params(),
      success: this.setReviews
    })
  }

  params() {
    return {
      page: this.state.page,
      page_size: this.state.pageSize,
      order: this.state.order,
      order_by: this.state.orderBy
    }
  }

  setReviews(response) {
    let data = response.data
    this.setState({
      reviews: data.items,
      totalEntries: data.total_entries,
      page: data.page,
      pageSize: data.page_size,
      loading: false
    })
  }

  onOrderChange(event) {
    this.setState({
      order: event.currentTarget.value,
      loading: true
    }, this.fetchReviews)
  }

  onPageChange(event) {
    this.setState({
      page: event.currentTarget.dataset.page,
      loading: true
    }, this.fetchReviews)
  }

  render(){
    if (this.state.loading) {
      return(
        <Loader text='Loading Reviews'/>
      )
    }else{
      return(
        <div className='full-width'>
          <StoreCreateReview storeId={this.props.storeId} />
          <div className='store-review-info columns is-gapless'
               style={{margin: '0.5rem 0'}}>
            <div className='column is-narrow'>
              <em>
                {this.state.totalEntries} {' '}
                {this.state.totalEntries == 1 ?
                  <span>review</span>
                :
                  <span>reviews</span>
                }
              </em>
            </div>

            <div className='column is-hidden-mobile'/>

            <div className='column is-narrow'>
              <SelectControl value={this.state.order}
                             onChange={this.onOrderChange}
                             options={OrderOptions} />
            </div>

          </div>

          <div className='store-reviews'>
            {this.state.reviews.map((review) => (
              <StoreReview key={review.id} review={review} />
            ))}
          </div>
          <Pagination pageChange={this.onPageChange}
                      totalEntries={this.state.totalEntries}
                      pageSize={this.state.pageSize}
                      page={this.state.page}
											style={{margin: '0.5rem 0'}}/>

        </div>
      )
    }
  }


}
