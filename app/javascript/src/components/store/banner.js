import React from 'react'
import StoreTag from 'components/store/tag'
import googleMapURLGenerator from 'util/google_map_url_generator'

const headerStyle = {
  fontSize: '2rem',
  fontWeight: '900'
}

const overlayStyle = {
  backgroundColor: 'rgba(0,0,0, 0.60)',
  color: '#fff',
  height: '100%',
  width: '100%',
  padding: '3rem 1.5rem',
  position: 'absolute'
}

const backgroundStyle = (storeId) => {
 let coverImageUrl = `/api/stores/${storeId}/cover_image`
 return {
  backgroundImage: `url(${coverImageUrl})`,
  backgroundSize: 'cover',
  backgroundRepeat: 'no-repeat',
  backgroundPosition: 'center center',
  minHeight: '350px'
 }
}

const storeAddress = (address) => {
  let addressUrl = googleMapURLGenerator(address)
  return(
  <a href={googleMapURLGenerator(address)} className='color-white'>
    {address.line_1}, {address.municipality}, {address.region}
  </a>)
}

const StoreBanner = ({store, loading}) => {
  return(
    <section id='store-banner-section'
             className='section'
             style={backgroundStyle(store.id)}>
      <div style={overlayStyle}>
        <div className='full-size dead-center-wrapper'>
          {loading ?
            <h1 style={headerStyle}>
              Loading Store Data
            </h1>
            :
            <div>
              <h1 style={headerStyle}>
                {store.name}
              </h1>
              {store.tags.map((tag) => (
                <StoreTag key={tag.id} tag={tag}/>
              ))}
              <div style={{paddingTop: '0.25rem'}}>
                <span className='icon'>
                  <i className='fas fa-map-marker-alt'
                     style={{paddingRight: '0.25rem'}}/>
                </span>
                <span>{storeAddress(store.address)}</span>
              </div>
              {store.phone &&
              <a href={`tel:${store.phone}`}
                 className='color-white'>
                <div style={{paddingTop: '0.25rem'}}>
                  <span className='icon'>
                    <i className='fas fa-phone'
                        style={{paddingRight: '0.25rem'}}/>
                  </span>
                  <span>{store.phone}</span>
                </div>
              </a>
              }
              <div>
                {store.page_view_count} View{store.page_view_count != 1 && 's'}
              </div>
            </div>
          }
        </div>
      </div>
    </section>
  )
}

export default StoreBanner
