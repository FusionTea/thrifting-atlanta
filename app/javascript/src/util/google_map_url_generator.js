const baseGoogleMapUrl = 'https://www.google.com/maps/dir/'

const googleMapsUrlSafe = (string) => {
  if (string == null || string == undefined){
    return ''
  }
  else{
    return string.replace(/[^a-z0-9]/gi, '+')
  }
}

const gmusf = googleMapsUrlSafe

const googleMapURLGenerator = ({line_1, municipality, region, postal_code}) => {
  return `${baseGoogleMapUrl}${gmusf(line_1)},+${gmusf(municipality)},+${gmusf(region)}+${gmusf(postal_code)}`
}

export default googleMapURLGenerator
