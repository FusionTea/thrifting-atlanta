const debounce = (func, wait) => {
  let timeout = null;
  return function() {
    let context = this
    let args = arguments
    clearTimeout(timeout)
    timeout = setTimeout(function(){
      func.apply(context, args)
    }, wait)
  }
}

export default debounce
