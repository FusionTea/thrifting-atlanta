const mobileMax = 768;
const tabletMin = 769;
const tabletMax = 1023;
const desktopMin = 1024;
const desktopMax = 1215;
const wideScreenMin = 1216;
const wideScreenMax = 1407;
const fullHdMin = 1408;

export const IsMobile = ()=> (
  screen.width <= mobileMax
)

export const NotMobile = () => (
  !IsMobile()
)
