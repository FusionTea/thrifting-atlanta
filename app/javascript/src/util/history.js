import 'url-search-params-polyfill'
import createHistory from 'history/createBrowserHistory'
import {withRouter} from 'react-router-dom'

const history = createHistory()

const isParamsEmpty = (params) => (
  Object.getOwnPropertyNames(params).length == 0
)

const generateQueryString = (params) => {
  let urlParams = new URLSearchParams()
  Object.entries(params)
        .forEach(([param, value]) => {
          if (value){
            urlParams.set(param, value)
          }
        })
  return urlParams.toString()
}

export const generateUrl = (url, queryParams = {}) => {
  if(isParamsEmpty(queryParams)){
    return url
  }else{
    return `${url}?${generateQueryString(queryParams)}`
  }
}

export const replaceHistory = (baseUrl, queryParams = {}) => {
  let url = generateUrl(baseUrl, queryParams)
  history.replace(url)
}

export const navigateTo = (baseUrl, queryParams = {}) => {
  let url = generateUrl(baseUrl, queryParams)
  history.push(url)
}

export const goBack = () => (history.goBack())

export default history
