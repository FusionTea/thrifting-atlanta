const isBlank = (data) => {
  if (data == undefined || data == null) {
    return true
  }
  if (typeof(data) == 'string'){
    return isStringBlank(data)
  }
  if(typeof(data) == 'boolean'){
    return false
  }
  if (typeof(data) == 'object'){
    return data.keys().length > 0
  }
}

const isStringBlank = (string) => (
  string.trim() == ''
)

export default isBlank
