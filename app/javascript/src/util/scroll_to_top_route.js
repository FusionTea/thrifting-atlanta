import React, {Component} from 'react'
import {Route, withRouter} from 'react-router-dom'

class ScrollToTopRoute extends Component {

  componentDidUpdate(prevProps){
    if(this.props.computedMatch.isExact){
      window.scrollTo(0,0)
    }
  }

  render() {
    return <Route {...this.props}/>
  }
}

export default withRouter(ScrollToTopRoute)
