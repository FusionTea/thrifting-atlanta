const codify = (string) => (
  string.toLowerCase()
      .replace(/^\s+|\s+$/g,'')
      .replace(/[^0-9a-z ]/g, '')
      .replace(/\s+/g, '_')
)

export default codify
