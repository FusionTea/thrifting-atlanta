const OnDomContentLoad = (func) => {
  if (document.readyState == 'interactive'){
    func()
  }else{
    document.addEventListener('DOMContentLoaded', func)
  }
}

export default OnDomContentLoad
