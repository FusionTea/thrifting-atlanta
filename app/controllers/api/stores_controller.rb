class Api::StoresController < ApplicationController
  DEFAULT_COVER_IMAGE_PATH = '/default-cover-image.png'

  def index
    json = StoresQuery.json(index_params)
    render json: json
  end

  def show
    store = Store.find(params[:id])
    store.page_view_count += 1
    render json: store
    store.save
  end

  def create
    builder = PendingStoreBuilder.build(create_params)
    if builder.valid?
      render json: builder.store
    else
      options = {status: 400, data: builder.store}
      message = builder.errors.full_messages.join(', ')
      raise ApiError.new(options), message
    end
  end

  def show_code
    store = Store.find_by_code(params[:code])
    store.page_view_count += 1
    render json: store
    store.save
  end

  def cover_image
    store = Store.find(params[:id])
    if store.cover_image.attached?
      redirect_to store.cover_image_url
    else
      redirect_to DEFAULT_COVER_IMAGE_PATH
    end
  end

  private

  def index_params
    params.merge(status: :active)
  end

  def create_params
    params.permit(:name, :description, :phone,
      :email, :website_url, :facebook_url,
      :twitter_url, :instagram_url, :latitude, :longitude,
      :business_hour_info, tag_ids: [], address_attributes: [
        :line_1, :line_2, :municipality,
        :region, :postal_code
      ])
  end

end
