class Api::RegistrationsController < Devise::RegistrationsController

  def create
    build_resource(sign_up_params)
    resource.save
    if resource.persisted?
      sign_up(resource_name, resource)
      render json: resource, status: 200
    else
      message = resource.errors.full_messages.join(', ')
      raise ApiError.new(data: base_sign_up_params), message
    end
  end

  private

  def after_sign_up_path_for(resource)
    root_path
  end

  def sign_up_params
    base_sign_up_params.tap do |p|
      p[:role] = :member
    end
  end

  def base_sign_up_params
    params.permit(:email, :password,
                    :password_confirmation,
                    :first_name, :last_name)
  end

end
