class Api::Admin::StorePhotosController < ApplicationController
  MISSING_CREATE_PARAMS_MSG = 'Missing store id or image'
  before_action :authenticate_admin!

  def create
    if params[:store_id].blank? || params[:image].blank?
      raise ApiError.new(status: 422),
              MISSING_CREATE_PARAMS_MSG
    end

    photo_builder = PhotoBuilder.build(params)

    if photo_builder.valid?
      store_photo = StorePhoto.create!(
        store_id: params[:store_id],
        photo: photo_builder.photo
      )
      render json: store_photo
    else
      messages = photo_builder.errors
                              .full_messages
                              .join(', ')
      raise ApiError.new(status: 400), messages
    end

  end

  def destroy
    store_photo = StorePhoto.find(params[:id])
    store_photo.destroy
    render json: store_photo
  end

  private

end
