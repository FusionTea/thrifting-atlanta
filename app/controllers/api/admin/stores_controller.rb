class Api::Admin::StoresController < ApplicationController
  before_action :authenticate_admin!
  before_action :set_store, only: [:update, :show, :update_cover_image]

  def index
    json = StoresQuery.json(params)
    render json: json
  end

  def update
    if @store.update(update_params)
      @store.cover_image.attach(params[:cover_image]) if params[:cover_image].present?
      StoreTsvBuilder.build(store: @store)
      render json: @store
    else
      options = {status: 422, data: @store}
      message = @store.errors.full_messages.join('; ')
      raise ApiError.new(options), message
    end
  end

  def update_cover_image
    unless params[:cover_image].present?
      raise ApiError.new(status: 422), 'No File Found'
    end
    @store.cover_image.attach(params[:cover_image])
    render json: @store
  end

  def show
    render json: @store
  end

  def create
    store = Store.new(create_params)
    if store.save
      StoreTsvBuilder.build(store: store)
      render json: store, status: 200
    else
      options = {status: 400, data: store}
      message = store.errors.full_messages.join('; ')
      raise ApiError.new(options), message
    end
  end

  private

  def set_store
    @store = Store.find(params[:id])
  end

  def update_params
    params.permit(:name, :description, :status, :phone,
                  :email, :website_url, :facebook_url,
                  :twitter_url, :instagram_url, :featured,
                  :latitude, :longitude, :business_hour_info,
                  tag_ids: [], address_attributes: [
                    :id, :line_1, :line_2, :municipality,
                    :region, :postal_code
                  ])
  end

  def create_params
    params.permit(:name, :code, :latitude, :longitude,
                  address_attributes: [
                    :line_1, :municipality, :region, :postal_code
                  ])
  end

end
