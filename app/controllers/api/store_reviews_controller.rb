class Api::StoreReviewsController < ApplicationController

  def index
    json = ReviewsQuery.json(index_params)
    render json: json
  end

  def create
    review = Review.new(create_params)
    if review.save
      render json: review, status: 200
    else
      options = {status: 400, data: review}
      message = review.errors.full_messages.join('; ')
      raise ApiError.new(options), message
    end
  end

  private

  def index_params
    params.merge(status: :approved)
  end

  def create_params
    params.permit(:store_id, :content,
    :reviewer_name, :reviewer_email).merge({
      user: current_user
    })
  end

end
