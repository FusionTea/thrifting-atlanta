class Api::SessionsController < Devise::SessionsController
  include Devise::Controllers::Rememberable

  def create
    resource = User.find_for_database_authentication(email: params[:email])
    raise_invalid_params unless resource.present?

    if resource.valid_password?(params[:password])
      sign_in :user, resource
      remember_me(resource) if should_remember?
      render json: resource, status: 200
    else
      raise_invalid_params
    end
  end

  def destroy
    raise ApiError.new(), 'No Login' if current_user.blank?

    signed_out = sign_out current_user
    if signed_out
      render json: current_user, status: 200
    else
      raise ApiError.new(), 'Could Not Logout'
    end
  end

  private

  def raise_invalid_params
    raise ApiError.new(status: 400), 'Email/Password is incorrect'
  end

  def should_remember?
    params[:remember_me]
  end

end
