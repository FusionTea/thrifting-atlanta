class Api::PasswordsController < Devise::PasswordsController
  skip_before_action :verify_authenticity_token
  layout 'passwords'

  def create_reset
    user = User.send_reset_password_instructions(create_params)
    if successfully_sent?(user)
      json = {successful: true, email: params[:email]}
      render json: json, status: 200
    else
      error_msg = 'Could not reset password for given email'
      data = {successful: false, email: params[:email]}
      raise ApiError.new(data: data), error_msg
    end
  end

  private

  def create_params
    params.permit(:email)
  end

end
