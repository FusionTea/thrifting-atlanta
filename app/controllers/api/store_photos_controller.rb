class Api::StorePhotosController < ApplicationController

  def index
    store_photos = StorePhoto.by_store(params[:store_id])
                             .order(created_at: :asc)
    render json: store_photos
  end

  def thumbnail
    store_photo = StorePhoto.find(params[:id])
    redirect_to store_photo.thumbnail_url
  end

  def show
    store_photo = StorePhoto.find(params[:id])
    redirect_to store_photo.image_url
  end

end
