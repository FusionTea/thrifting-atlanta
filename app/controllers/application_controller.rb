class ApplicationController < ActionController::Base
  COLLECTION_SERIALIZER = ActiveModel::Serializer::CollectionSerializer

  rescue_from ApiError do |e|
    render json: e.json, status: e.status
  end

  private

  def authenticate_admin!
    authenticate_user!
    return if current_user.admin?

    raise ApiError.new(status: 401), 'User must be admin'
  end

  def authenticate_user!(resource = nil)
    resource ||= current_user
    return if resource.present?

    raise ApiError.new(status: 401), 'Must be signed in'
  end

end
