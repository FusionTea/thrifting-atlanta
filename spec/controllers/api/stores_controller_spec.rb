require 'rails_helper'

RSpec.describe Api::StoresController, type: :controller do

  describe '#create' do
    let!(:admin) {create(:admin)}

    context 'valid params' do
      let(:params) {{
        name: 'store name',
        description: 'store description',
        website_url: 'website_url',
        facebook_url: 'facebook_url',
        twitter_url: 'twitter_url',
        instagram_url: 'instagram_url',
        phone: '312-123-1234',
        address_attributes: {
          line_1: "815 West weed st.",
          municipality: 'Chicago',
          region: 'IL',
          postal_code: '60642'
        }
      }}

      it 'returns the pending store' do
        post :create, params: params
        expect(response.status).to eq 200
        json = JSON.parse(response.body)
        expect(json['id']).not_to be nil
        expect(json['name']).to eq params[:name]
      end

    end

    context 'invalid params' do
      let(:params) {{
        name: 'A store with invalid params'
      }}


      it 'raises an api error' do
        post :create, params: params
        expect(response.status).to eq 400
        json = JSON.parse(response.body)
        expect(json['message']).not_to be nil
      end

    end

  end
  describe '#index' do
    let!(:store1) {create(:store, status: :active)}
    let!(:store2) {create(:store, status: :hidden)}
    let!(:store3) {create(:store, status: :archived)}

    it 'returns only active stores' do
      expect(Store.count).to eq 3
      get :index
      expect(response.status).to eq 200
      json = JSON.parse(response.body)
      expect(json['items'].length).to eq Store.by_status('active').count
      expect(json['items'].first['id']).to eq store1.id
    end

    context 'sending params' do
      let!(:store4) {create(:store, featured: true)}
      let!(:store5) {create(:store, featured: false)}

      context 'features is true' do
        let(:params) {{featured: true}}

        it 'can return only featured stores' do
          get :index, params: params
          expect(response.status).to eq 200
          json = JSON.parse(response.body)
          expect(json['items'].length).to eq Store.featured.count
          expect(json['items'].first['id']).to eq store4.id
        end

      end

      context 'store code is passed' do
        let(:params) {{code: store1.code}}

        it 'can return stores matching unique code' do
          get :index, params: params
          expect(response.status).to eq 200
          json = JSON.parse(response.body)
          expect(json['items'].first['id']).to eq store1.id
        end

      end

    end

  end

end
