require 'rails_helper'

RSpec.describe Api::StoreReviewsController, type: :controller do

  describe '#index' do
    let!(:store1) {create(:store)}
    let!(:review1a) {create(:review, status: :approved, store: store1)}
    let!(:review1b) {create(:review, status: :flagged, store: store1)}
    let!(:store2) {create(:store)}
    let!(:review2a) {create(:review, status: :approved, store: store2)}
    let(:params) {{
      store_id: store1.id
    }}

    it 'returns all of the active reviews for the specified store' do
      get :index, params: params
      expect(response.status).to eq 200
      json = JSON.parse(response.body)
      expect(json['total_entries']).to eq 1
      expect(json['items'][0]['id']).to eq review1a.id
    end

  end

  describe '#create' do
    let(:store) {create(:store)}
    let(:params) {{
      store_id: store.id,
      content: "HELLO WORLD",
      reviewer_name: 'reviewer',
      reviewer_email: 'reviewer@email.com'
    }}

    it 'creates the review' do
      post :create, params: params
      expect(response.status).to eq 200
      json = JSON.parse(response.body)
      expect(json['id']).to eq Review.first.id
    end

    context 'current user is signed in' do
      let(:user) {create(:user)}
      let(:params) {{
        store_id: store.id,
        content: 'HELLO WORLD',
      }}

      it 'automagically attaches the current user to the created review' do
        controller.stub(:current_user).and_return(user)
        post :create, params: params
        expect(response.status).to eq 200
        expect(Review.first.user_id).to eq user.id
      end

    end

  end

end
