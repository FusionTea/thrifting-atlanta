require 'rails_helper'

RSpec.describe Api::Admin::StorePhotosController, type: :controller do

  describe '#create' do
    let(:image) {
      fixture_file_upload('spec/test_files/photo.jpg',
      'image/jpg')
    }
    let(:store) {create(:store)}
    let(:admin) {create(:admin)}
    let(:description) {'image description'}
    let(:params) {{
      image: image,
      store_id: store.id,
      description: description
    }}


    context 'user is not type admin' do
      let(:user) {create(:user)}

      it 'returns a 401 response' do
        controller.stub(:current_user).and_return(user)
        post :create, params: params
        expect(response.status).to eq 401
      end

    end

    context 'no store id is passed in' do
      let(:params) {{
        image: image
      }}

      it 'returns a 422 response' do
        controller.stub(:current_user).and_return(admin)
        post :create, params: params
        expect(response.status).to eq 422
      end

    end

    context 'no image is uploaded' do
      let(:params) {{
        store_id: store.id
      }}

      it 'returns a 422 response' do
        controller.stub(:current_user).and_return(admin)
        post :create, params: params
        expect(response.status).to eq 422
      end

    end

    context 'image is not type of image' do
      let(:image) {
        fixture_file_upload('spec/test_files/text.txt',
        'text/plain')
      }

      it 'returns a 422 response' do
        controller.stub(:current_user).and_return(admin)
        post :create, params: params
        expect(response.status).to eq 400
      end

    end

    context 'valid params are provided' do

      it 'creates the store image' do
        controller.stub(:current_user).and_return(admin)
        post :create, params: params
        expect(response.status).to eq 200
        json = JSON.parse(response.body)
        expect(StorePhoto.count).to be 1
        store_photo = StorePhoto.first
        expect(json['id']).to eq store_photo.id
        expect(json['description']).to eq store_photo.description
      end

    end

  end

end
