require 'rails_helper'

RSpec.describe Api::Admin::StoresController, type: :controller do

  describe '#update' do
    let(:store) {create(:store)}
    let(:admin) {create(:admin)}
    let(:params) {{
      id: store.id,
      name: 'hello thrift',
      description: 'best store description'
    }}

    it 'udpates the store' do
      expect(StoreTsvBuilder).to receive(:build)
      controller.stub(:current_user).and_return(admin)
      post :update, params: params
      expect(response.status).to eq 200
    end

  end
end
