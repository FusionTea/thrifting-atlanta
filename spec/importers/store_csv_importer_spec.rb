require 'rails_helper'

RSpec.describe StoreCsvImporter do


  describe '#self.import_string' do
    before(:all) do
      @csv_string = IO.read('spec/test_files/store-import.csv')
    end

    it 'should import stores from the csv' do
      importer = described_class.import_string(@csv_string)
      expect(importer.imported_stores.length).to eq 3
    end

    it 'also sets the tsv for all the store' do
      importer = described_class.import_string(@csv_string)
      store = importer.imported_stores.first
      expect(store.tsv).not_to be nil
    end


    context 'line has unique code' do

      it 'should create store with unique code' do
        importer = described_class.import_string(@csv_string)
        expect(Store.exists?(code: :the_greatest_store)).to be true
      end

    end

    context 'line does not have unique code' do
      let(:name) {'A Neat Boutique, Inc.'}

      it 'creates a unique code based on the store name and address' do
        importer = described_class.import_string(@csv_string)
        expect(importer.imported_stores.length).to eq 3
        store = Store.find_by_name(name)

        expected_code = "#{store.name} #{store.address.line_1} "\
          "#{store.address.municipality} #{store.address.postal_code}"
          .downcase.strip.gsub(/[^0-9a-z]/i, '_')
        expect(store.code).to eq expected_code
      end

    end

    context 'store already exists' do
      let!(:code) {:the_greatest_store}
      let!(:existing_store) {create(:store,
        code: code
      )}

      it 'should find the store by the unique code and update it' do
        old_attributes = existing_store.attributes
        old_addr_attrs = existing_store.address.attributes
        importer = described_class.import_string(@csv_string)
        updated_store = Store.find_by_code(code)
        expect(old_attributes).not_to eq updated_store.attributes
        expect(old_addr_attrs['id']).to eq updated_store.address.id
        expect(updated_store.address.line_1).to eq '815 West Weed St.'
        expect(updated_store.address.line_2).to eq 'Ground Floor: Attn: "TimeDoc"'
        expect(updated_store.address.municipality).to eq 'Chicago'
        expect(updated_store.address.region).to eq 'IL'
        expect(updated_store.address.postal_code).to eq '60642'
        expect(updated_store.address.country).to eq 'US'
      end

    end

  end

end
