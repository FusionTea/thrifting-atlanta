require 'rails_helper'

RSpec.describe ReviewsQuery do
  let!(:store1) {create(:store)}
  let!(:review1a) {create(:review, store: store1, status: :pending)}
  let!(:review1b) {create(:review, store: store1, status: :flagged)}
  let!(:review1c) {create(:review, store: store1, status: :approved)}

  let!(:store2) {create(:store)}
  let!(:review2a) {create(:review, store: store2, status: :approved)}


  it 'can apply filters and paginate' do
    query = described_class.new
    expect(query.total_entries).to eq Review.count

    query = described_class.new(store_id: store1.id, page_size: 2, page: 1)
    expect(query.total_entries).to eq store1.reviews.count
    expect(query.reviews.count).to eq 2

    query = described_class.new(store_id: store1.id, page_size: 2, page: 1, status: :approved)
    expect(query.total_entries).to eq 1
    expect(query.reviews.first.id).to eq review1c.id
  end

end
