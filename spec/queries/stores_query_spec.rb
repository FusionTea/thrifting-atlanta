require 'rails_helper'

RSpec.describe StoresQuery do
  let!(:store1) {create(:store, featured: true)}
  let!(:store2) {create(:store, featured: false)}
  let!(:store3) {create(:store, featured: false)}

  context 'no params are passed' do

    it 'returns all stores' do
      query = described_class.new
      expect(query.stores.count).to eq Store.count
    end

  end

  context 'featured is true' do

    it 'only returns stores that are featured' do
      query = described_class.new(featured: true)
      expect(query.total_entries).to eq 1
      expect(query.stores.first).to eq store1
    end

  end

  context 'code is passed' do

    it 'only returns stores that match that code' do
      query = described_class.new(code: store1.code)
      expect(query.total_entries).to eq 1
      expect(query.stores.first).to eq store1
    end

  end

  context 'region filter applied' do

    it 'returns store in the selected region' do
      Address.all.update_all(region: 'FL')
      store3.address.update(region: 'IL')
      query = described_class.new(region: 'IL')
      expect(query.total_entries).to eq 1
      expect(query.stores.first).to eq store3
    end

  end

  context 'status filter is active' do

    it 'returns active stores' do
      Store.all.update_all(status: :hidden)
      store2.reload.update(status: :active)
      query = described_class.new(status: 'active')
      expect(query.total_entries).to eq 1
      expect(query.stores.first).to eq store2
    end

  end

end
