FactoryBot.define do

  factory :address do
    name 'Home Address'
    line_1 Faker::Address.street_address
    municipality Faker::Address.city
    region Faker::Address.state_abbr
    postal_code Faker::Address.zip
    country Faker::Address.country
  end

  factory :store do
    name Faker::Company.name
    description Faker::Company.catch_phrase
    status 'active'
    featured false
    website_url 'https://www.beststore.com'
    instagram_url 'https://instagram.com/beststore'
    facebook_url 'https://facebook.com/beststore'
    twitter_url 'https://twitter.com/beststore'
    sequence(:code){|n| "code_#{n}"}
    address
  end

  factory :tag do
    name Faker::Commerce.department
  end

  factory :store_tag do
    store
    tag
  end

  factory :user do
    first_name Faker::Name.first_name
    last_name Faker::Name.last_name
    sequence(:email){|n| "#{n}_email@email.com"}
    password 'Password1'
    password_confirmation 'Password1'
    role :member

    factory :admin do
      role :admin
    end

  end

  factory :review do
    store
    user {create(:user)}
    content 'REVIEW CONTENT'
    status :approved
  end

end
