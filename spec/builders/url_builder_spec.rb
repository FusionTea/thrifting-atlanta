require 'rails_helper'

RSpec.describe UrlBuilder do

  describe '#self.build' do

    context 'only path is provided' do
      let(:params) {{
        path: 'hello/world'
      }}
      let(:url) {described_class.build(params)}

      it 'uses the mailer default url options' do
        expect(url).to include(ActionMailer::Base.default_url_options[:host])
        expect(url).to eq 'http://localhost:5000/hello/world'
      end

    end

    context 'other protocol and host is provided' do
      let(:params) {{
        protocol: 'https',
        host: 'www.google.com',
        path: 'hello/google',
        port: ''
      }}
      let(:url) {described_class.build(params)}

      it 'will utilize the passed params' do
        expect(url).to eq 'https://www.google.com/hello/google'
      end

    end

  end
end
