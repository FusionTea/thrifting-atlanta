require 'rails_helper'

RSpec.describe StoreTsvBuilder do

  describe '#self.build' do
    let!(:tag1) {create(:tag, name: 'Thrift')}
    let!(:tag2) {create(:tag, name: 'Consignment')}
    let!(:store) {create(:store)}
    let!(:store_tag1) {create(:store_tag, store: store, tag: tag1)}
    let!(:store_attributes) {
      store.attributes.slice('name', 'description').values.join(' ')
    }
    let!(:address_attributes) {
      store.address.attributes
        .slice('municipality', 'postal_code')
        .values.join(' ')
    }
    let!(:tag_attributes) {
      store.tags.pluck(:name).join(' ')
    }
    let!(:tsv_string) {
      ActiveRecord::Base.connection.quote_string(
        "#{store_attributes} #{address_attributes} #{tag_attributes}"
      )
    }
    let!(:expected_tsv) {
      ActiveRecord::Base.connection.execute(
        "SELECT to_tsvector('english', '#{tsv_string}') as tsv"
      ).first['tsv']
    }

    it 'sets a tsvector on colum including store metadata, tags, and address' do
      described_class.build(store: store)
      expect(store.tsv).to eq expected_tsv
    end

    context 'store description has quotes' do
      let!(:store) {create(:store, description: "store's best description")}

      it 'will not fail on description with quotes' do
        described_class.build(store: store)
        expect(store.tsv).to eq expected_tsv
      end

    end

    context 'store description has double quotes' do
      let!(:store) {create(:store, description: "They call us the \"best\"")}

      it 'will not fail' do
        described_class.build(store: store)
        expect(store.tsv).to eq expected_tsv
      end

    end

  end

end
