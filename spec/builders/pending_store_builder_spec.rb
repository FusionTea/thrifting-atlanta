require 'rails_helper'

RSpec.describe PendingStoreBuilder do

  describe '#self.build' do
    let(:params) {{
      name: 'store name',
      description: 'store description',
      website_url: 'website_url',
      facebook_url: 'facebook_url',
      twitter_url: 'twitter_url',
      instagram_url: 'instagram_url',
      phone: '312-123-1234',
      address_attributes: {
        line_1: "815 West weed st.",
        municipality: 'Chicago',
        region: 'IL',
        postal_code: '60642'
      }
    }}
    let(:builder) {described_class.build(params)}
    let!(:admin) {create(:admin)}

    it 'should create the store with the appropriate params' do
      expect(builder.store).to be_valid
      expect(builder.store.status).to eq 'pending'
    end

    it 'should send an email to the admin' do
      expect{builder}.to change {ActionMailer::Base.deliveries.count}.by 1
    end

  end

end
