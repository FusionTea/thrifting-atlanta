require 'rails_helper'

RSpec.describe StoreAddressCoordinateBuilder do

  describe '#self.build' do
    let(:store) {create(:store,
      address_attributes: {
        line_1: '814 South Bell Ave',
        municipality: 'Chicago',
        region: 'IL',
        postal_code: '60642'
      }
    )}

    it 'assigns longitude and latitude to a store' do
      VCR.use_cassette('testing address geocoding') do
        builder = described_class.build(store)
        store.reload
        expect(store.latitude).to eq builder.lat
        expected_lng = BigDecimal(builder.lng.round(6).to_s)
        expect(BigDecimal(store.longitude)).to eq expected_lng
      end
    end

  end

  describe '#self.build_batch' do
    let(:store1) {create(:store,
      address_attributes: {
        line_1: '814 South Bell Ave',
        municipality: 'Chicago',
        region: 'IL',
        postal_code: '60612'
      }
      )}
    let(:store2) {create(:store,
      address_attributes: {
        line_1: '815 West Weed St',
        municipality: 'Chicago',
        region: 'IL',
        postal_code: '60642'
      }
    )}

    it 'shuold assign latitude and longitudes for an array of stores' do
      expect(described_class).to receive(:build).twice
      described_class.build_batch([store1, store2])
    end

  end

end
