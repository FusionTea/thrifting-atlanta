require 'rails_helper'

RSpec.describe Store do

  describe 'validations' do

    it 'enforces codes on stores to be unique' do
      non_unique_code = 'not_unique'
      store1 = create(:store, code: non_unique_code)
      store2 = build(:store, code: non_unique_code)
      expect(store2).not_to be_valid
    end

    it 'forces codes on a store to be urlsafe' do
      store = create(:store)
      non_valid_code = 'this is a non valid code'
      store.code = non_valid_code
      expect(store).not_to be_valid
    end

    it 'can create a store with a pending status' do
      store = build(:store, status: :pending)
      expect(store).to be_valid
    end

  end

end
