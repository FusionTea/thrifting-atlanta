require 'rails_helper'

RSpec.describe Review, type: :model do

  describe 'validations' do

    it 'does not require a user' do
      review= build(:review, user: nil)
    end

    it 'requires content' do
      review = build(:review, content: "   ")
      expect(review).not_to be_valid

      review.content = 'I am no longer empty :)'
      expect(review).to be_valid
    end

    it 'requires a store' do
      review = build(:review, store: nil)
      expect(review).not_to be_valid

      review.store = create(:store)
      expect(review).to be_valid
    end

  end

end
