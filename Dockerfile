# This Dockerfile compiles the makigas web application. It can be
# used to deploy a static image to a production server or to
# develop or test the application in a standalone application
# without having to install the stuff.
ARG rails_env=production
ARG node_env=production
FROM ruby:2.4.1-alpine

RUN apk add --update alpine-sdk postgresql-dev imagemagick nodejs tzdata && \
		npm install -g yarn

RUN mkdir /app
WORKDIR /app

COPY Gemfile* /app/
RUN gem install bundler && bundle install

COPY package.json /app/
COPY yarn.lock /app/
RUN yarn install

COPY . /app

ENV RAILS_ENV production
ENV NODE_ENV production
ENV RAILS_SERVE_STATIC_FILES 1
RUN bin/rails assets:precompile
RUN bin/rails webpacker:compile

EXPOSE 3000
CMD bin/rails s
