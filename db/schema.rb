# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_08_09_003600) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "btree_gin"
  enable_extension "plpgsql"

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "addresses", force: :cascade do |t|
    t.string "name", null: false
    t.string "organization_name"
    t.string "line_1", null: false
    t.string "line_2"
    t.string "municipality", null: false
    t.string "sub_region"
    t.string "region", null: false
    t.string "postal_code"
    t.string "country", null: false
  end

  create_table "photos", force: :cascade do |t|
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "reviews", force: :cascade do |t|
    t.integer "user_id"
    t.integer "store_id"
    t.string "status"
    t.text "content"
    t.string "approved_datetime"
    t.string "flagged_datetime"
    t.datetime "date"
    t.string "reviewer_name"
    t.string "reviewer_email"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "store_photos", force: :cascade do |t|
    t.integer "store_id"
    t.integer "photo_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["photo_id"], name: "index_store_photos_on_photo_id"
    t.index ["store_id"], name: "index_store_photos_on_store_id"
  end

  create_table "store_tags", force: :cascade do |t|
    t.integer "store_id"
    t.integer "tag_id"
    t.index ["store_id", "tag_id"], name: "index_store_tags_on_store_id_and_tag_id", unique: true
    t.index ["store_id"], name: "index_store_tags_on_store_id"
    t.index ["tag_id"], name: "index_store_tags_on_tag_id"
  end

  create_table "stores", force: :cascade do |t|
    t.string "name", null: false
    t.string "code", null: false
    t.text "description"
    t.string "status", null: false
    t.integer "page_view_count", default: 0, null: false
    t.string "phone"
    t.string "email"
    t.text "website_url"
    t.text "facebook_url"
    t.text "instagram_url"
    t.text "twitter_url"
    t.boolean "featured", default: false, null: false
    t.integer "address_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.tsvector "tsv"
    t.decimal "latitude"
    t.decimal "longitude"
    t.text "business_hour_info"
    t.index ["address_id"], name: "index_stores_on_address_id"
    t.index ["code"], name: "index_stores_on_code", unique: true
    t.index ["tsv"], name: "index_stores_on_tsv", using: :gin
  end

  create_table "tags", force: :cascade do |t|
    t.string "name", null: false
    t.index ["name"], name: "index_tags_on_name", unique: true
  end

  create_table "users", force: :cascade do |t|
    t.string "first_name", default: "", null: false
    t.string "last_name", default: "", null: false
    t.string "role", default: "", null: false
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "store_photos", "photos"
  add_foreign_key "store_photos", "stores"
  add_foreign_key "store_tags", "stores"
  add_foreign_key "store_tags", "tags"
  add_foreign_key "stores", "addresses"
end
