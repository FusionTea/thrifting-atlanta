class AddSearchIndexToStore < ActiveRecord::Migration[5.2]
  def up
    execute "CREATE EXTENSION IF NOT EXISTS btree_gin;"
    add_column :stores, :tsv, :tsvector
    add_index :stores, :tsv, using: :gin

    execute <<-SQL
      CREATE TRIGGER tsvectorupdate BEFORE INSERT OR UPDATE
      ON stores FOR EACH ROW EXECUTE PROCEDURE
      tsvector_update_trigger(
        tsv, 'pg_catalog.english', name, description, website_url, facebook_url, twitter_url, instagram_url
      );
    SQL

    now = Time.current.to_s(:db)
    update("UPDATE stores SET updated_at = '#{now}'")
  end

  def down
    execute <<-SQL
      DROP TRIGGER tsvectorupdate
      ON stores
    SQL

    remove_index :stores, :tsv
    remove_column :stores, :tsv
    execute "DROP EXTENSION IF EXISTS btree_gin;"
  end
end
