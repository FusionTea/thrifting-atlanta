class CreateStore < ActiveRecord::Migration[5.2]
  def change
    create_table :addresses do |t|
      t.string :name, null: false
      t.string :organization_name
      t.string :line_1, null: false
      t.string :line_2
      t.string :municipality, null: false
      t.string :sub_region
      t.string :region, null: false
      t.string :postal_code
      t.string :country, null: false
    end

    create_table :stores do |t|
      t.string :name, null: false
      t.string :code, null: false
      t.text :description
      t.string :status, null: false
      t.integer :page_view_count, null: false, default: 0
      t.string :phone
      t.string :email
      t.text :website_url
      t.text :facebook_url
      t.text :instagram_url
      t.text :twitter_url
      t.boolean :featured, null: false, default: false
      t.integer :address_id, null: false

      t.timestamps
    end

    add_index :stores, :address_id
    add_index :stores, :code, unique: true

    add_foreign_key :stores, :addresses

    create_table :tags do |t|
      t.string :name, null: false
    end

    add_index :tags, :name, unique: true

    create_table :store_tags do |t|
      t.integer :store_id
      t.integer :tag_id
    end

    add_index :store_tags, :store_id
    add_index :store_tags, :tag_id
    add_index :store_tags, [:store_id, :tag_id], unique: true
    add_foreign_key :store_tags, :stores
    add_foreign_key :store_tags, :tags

  end
end
