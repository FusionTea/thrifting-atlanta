class DropTsvTrigger < ActiveRecord::Migration[5.2]

  def up
    execute <<-SQL
      DROP TRIGGER tsvectorupdate
      ON stores
    SQL
  end

  def down
    execute <<-SQL
      CREATE TRIGGER tsvectorupdate BEFORE INSERT OR UPDATE
      ON stores FOR EACH ROW EXECUTE PROCEDURE
      tsvector_update_trigger(
        tsv, 'pg_catalog.english', name, description, website_url, facebook_url, twitter_url, instagram_url
      );
    SQL

    now = Time.current.to_s(:db)
    update("UPDATE stores SET updated_at = '#{now}'")
  end

end
