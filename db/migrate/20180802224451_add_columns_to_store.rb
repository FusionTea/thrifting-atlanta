class AddColumnsToStore < ActiveRecord::Migration[5.2]
  def change
    add_column :stores, :latitude, :decimal
    add_column :stores, :longitude, :decimal
    add_column :stores, :business_hour_info, :text
  end
end
