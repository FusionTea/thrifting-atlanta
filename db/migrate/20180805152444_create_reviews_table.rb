class CreateReviewsTable < ActiveRecord::Migration[5.2]
  def change
    create_table :reviews do |t|
      t.integer :user_id
      t.integer :store_id
      t.string :status
      t.text :content
      t.string :approved_datetime
      t.string :flagged_datetime
      t.datetime :date
      t.string :reviewer_name
      t.string :reviewer_email

      t.timestamps
    end
  end
end
