class CreateStorePhotos < ActiveRecord::Migration[5.2]
  def change

    create_table :photos do |t|
      t.text :description

      t.timestamps
    end

    create_table :store_photos do |t|
      t.integer :store_id
      t.integer :photo_id

      t.timestamps
    end

    add_index :store_photos, :store_id
    add_index :store_photos, :photo_id

    add_foreign_key :store_photos, :stores
    add_foreign_key :store_photos, :photos

  end
end
